import {UPDATE_USER_SESSION} from 'src/store/action/auth';
// import EncryptedStorage from 'react-native-encrypted-storage';

type InitStateType = {
  userSession: {
    token: any;
    email: any;
    role: any;
  };
  isLogin: any;
};

type ActionType = {
  payload: any;
  type: typeof UPDATE_USER_SESSION;
};

export const initState: InitStateType = {
  userSession: {
    token: null,
    email: null,
    role: null,
  },
  isLogin: false,
};

export const reducer = (
  state: InitStateType,
  action: ActionType,
): InitStateType => {
  switch (action.type) {
    case UPDATE_USER_SESSION:
      state.userSession = action.payload.userSession;
      state.isLogin = action.payload.isLogin;
      return {
        userSession: state.userSession,
        isLogin: state.isLogin,
      };
    // case LOGOUT:
    //   return {
    //     userSession: {
    //       token: '',
    //       email: '',
    //       role: '',
    //     },
    //     isLogin: false,
    //   };
    default:
      return state;
  }
};
