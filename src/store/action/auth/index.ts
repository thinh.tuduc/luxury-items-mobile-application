export const GET_USER_SESSION = 'GET_USER_SESSION';
export const UPDATE_USER_SESSION = 'UPDATE_USER_SESSION';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
