import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  category_box_wrapper: {
    width: 104 * default_style.width_ratio,
    height: 64 * default_style.height_ratio,
    borderWidth: 1,
    borderColor: '#dcdcdc',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  category_box_margin: {
    marginRight: 10,
  },
  category_box_icon: {
    color: default_style.gray,
    textAlign: 'center',
  },
  category_box_text: {
    color: default_style.gray,
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
  },
});
