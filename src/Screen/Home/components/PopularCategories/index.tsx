import React from 'react';
import {Text, View, ScrollView} from 'react-native';
import {Icon} from '@ant-design/react-native';

import HomeItem from '../HomeItem';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

interface CategoryBoxProp {
  categoryName: string;
  iconName: any;
  isLastItem: boolean;
}

const Category_Box = ({
  categoryName,
  iconName,
  isLastItem,
}: CategoryBoxProp) => (
  <View
    style={
      isLastItem
        ? styles.category_box_wrapper
        : [styles.category_box_wrapper, styles.category_box_margin]
    }>
    <View>
      <Icon
        name={iconName}
        size={default_style.icon_size}
        style={styles.category_box_icon}
      />
      <Text style={styles.category_box_text}>{categoryName}</Text>
    </View>
  </View>
);

const category = [
  {name: 'Fashion', icon: 'skin'},
  {name: 'Real Estate', icon: 'shopping'},
  {name: 'Accessories', icon: 'sketch'},
  {name: 'Jewelry', icon: 'sketch'},
  {name: 'Jewelry', icon: 'sketch'},
];

const PopularCategories = () => {
  return (
    <>
      <HomeItem isViewAll title="Popular Categories">
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {category.map((x, id) => (
            <Category_Box
              key={id}
              isLastItem={id === category.length - 1 ? true : false}
              categoryName={x.name}
              iconName={x.icon}
            />
          ))}
        </ScrollView>
      </HomeItem>
    </>
  );
};

export default PopularCategories;
