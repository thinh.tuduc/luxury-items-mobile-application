import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  itemWrapper: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
  },
  text: {
    fontSize: default_style.lg,
    color: default_style.black,
    fontFamily: default_style.font_family_600,
    lineHeight: default_style.line_height_normal,
  },
  header: {
    marginBottom: 10,
  },
  view_all_text: {
    fontSize: default_style.md,
    color: default_style.black,
    fontFamily: default_style.font_family_normal,
    lineHeight: default_style.line_height_normal,
  },
});
