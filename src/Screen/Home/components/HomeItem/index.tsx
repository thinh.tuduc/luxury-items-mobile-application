import React from 'react';
import {Text, View} from 'react-native';
import {Flex} from '@ant-design/react-native';

import {styles} from './style';

interface HomeItemProp {
  children: React.ReactNode;
  title: string;
  isViewAll: boolean;
}

const title_margin = {marginBottom: 10};

const HomeItem = ({children, title, isViewAll}: HomeItemProp) => {
  return (
    <>
      <View style={styles.itemWrapper}>
        {isViewAll ? (
          <Flex justify="between" style={styles.header}>
            <Text style={styles.text}>{title}</Text>
            <Text style={styles.view_all_text}>View all</Text>
          </Flex>
        ) : (
          <Text style={[styles.text, title_margin]}>{title}</Text>
        )}
        {children}
      </View>
    </>
  );
};

export default HomeItem;
