import React, {useState, useRef} from 'react';
import {Image, ScrollView, View} from 'react-native';
import useInterval from 'src/utils/useInterval';

import HomeItem from '../HomeItem';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const images = [
  'https://www.meme-arsenal.com/memes/07430c5e04699631b5088be95b3d99cc.jpg',
  'https://d17fnq9dkz9hgj.cloudfront.net/uploads/2018/03/Russian-Blue_01.jpg',
  'https://c4.wallpaperflare.com/wallpaper/155/905/98/animals-1920x1200-funny-wallpaper-preview.jpg',
];

const Promotions = () => {
  const scrollRef = useRef<any>(null);
  const [currentImg, setCurrentImg] = useState(0);

  useInterval(() => handleAnimation(), 2000);

  const handleAnimation = () => {
    let newCur = currentImg + 1;
    if (newCur >= images.length) {
      newCur = 0;
    }
    scrollRef.current.scrollTo({
      x:
        (default_style.screen_width -
          2 * default_style.standard_padding_horizontal) *
        newCur,
      y: 0,
      animated: true,
    });
    setCurrentImg(newCur);
  };

  return (
    <>
      <HomeItem isViewAll={false} title="Promotions">
        <ScrollView
          ref={scrollRef}
          scrollEnabled={false}
          pagingEnabled
          horizontal
          showsHorizontalScrollIndicator={false}>
          {images.map((x, id: number) => (
            <Image
              key={id}
              source={{uri: x}}
              resizeMode="cover"
              style={styles.image}
            />
          ))}
        </ScrollView>
        <View style={styles.dotWrapper}>
          {images.map((x, id) => (
            <View
              key={id}
              style={[
                styles.dot,
                id === currentImg ? styles.active_dot : undefined,
              ]}
            />
          ))}
        </View>
      </HomeItem>
    </>
  );
};

export default Promotions;
