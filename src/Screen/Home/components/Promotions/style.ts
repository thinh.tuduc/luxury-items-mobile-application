import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  image: {
    width:
      default_style.screen_width -
      2 * default_style.standard_padding_horizontal,
    height: 110,
  },
  dotWrapper: {
    width: default_style.screen_width,
    position: 'absolute',
    flexDirection: 'row',
    left: '50%',
    bottom: 20,
    zIndex: 2,
  },
  dot: {
    width: 9,
    height: 9,
    borderRadius: 4.5,
    backgroundColor: default_style.white,
    marginBottom: 10,
    marginRight: 10,
  },
  active_dot: {
    backgroundColor: default_style.light_sun,
  },
});
