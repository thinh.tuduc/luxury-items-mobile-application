import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#faf8f6',
    height: '100%',
  },
  space: {
    width: '100%',
    height: 10,
    backgroundColor: '#faf8f6',
  },
});
