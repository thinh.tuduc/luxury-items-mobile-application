import React from 'react';
import {View, ScrollView} from 'react-native';

import Promotions from '../Promotions';
import PopularCategories from '../PopularCategories';
import NewArrival from '../NewArrival';

import {styles} from './style';

const MainHome = (props: any) => {
  return (
    <>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <Promotions />
        <View style={styles.space} />
        <PopularCategories />
        <View style={styles.space} />
        <NewArrival {...props} />
      </ScrollView>
    </>
  );
};

export default MainHome;
