import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import MasonryList from '@react-native-seoul/masonry-list';

import HomeItem from '../HomeItem';
import {styles} from './style';

interface CardProp {
  id: number;
  name: string;
  branch: string;
  price: string;
}

const NewArrival = (props: any) => {
  const Card = ({id, name, branch, price}: CardProp) => (
    <View
      style={
        id % 2 === 0
          ? [styles.card_wrapper, styles.card_margin]
          : styles.card_wrapper
      }>
      <View style={styles.image_wrapper} />
      <View style={styles.card_body_wrapper}>
        <Text style={styles.text_name}>{name}</Text>
        <Text style={styles.text_branch}>{branch}</Text>
        <Text style={styles.text_price}>{price}</Text>
      </View>
    </View>
  );
  const arrival = [
    {id: 1, name: 'Leather Slippers', branch: 'Vans', price: '1,200,000 VNĐ'},
    {
      id: 2,
      name: 'Oversized Woven Blazer - Red',
      branch: 'Versace',
      price: '1,200,000 VNĐ',
    },
    {
      id: 3,
      name: 'Oversized Woven Blazer - Red',
      branch: 'Versace',
      price: '1,200,000 VNĐ',
    },
    {id: 4, name: 'Leather Slippers', branch: 'Vans', price: '1,200,000 VNĐ'},
    {id: 5, name: 'Leather Slippers', branch: 'Vans', price: '1,200,000 VNĐ'},
    {
      id: 6,
      name: 'Oversized Woven Blazer - Red',
      branch: 'Versace',
      price: '1,200,000 VNĐ',
    },
    {
      id: 7,
      name: 'Oversized Woven Blazer - Red',
      branch: 'Versace',
      price: '1,200,000 VNĐ',
    },
    {id: 8, name: 'Leather Slippers', branch: 'Vans', price: '1,200,000 VNĐ'},
  ];
  return (
    <>
      <HomeItem isViewAll title="New Arrivals">
        <MasonryList
          data={arrival}
          numColumns={2}
          renderItem={({item, i}: any) => (
            <TouchableOpacity
              key={i}
              onPress={() => props.navigation.push('Product')}>
              <Card
                id={item.id}
                name={item.name}
                branch={item.branch}
                price={item.price}
              />
            </TouchableOpacity>
          )}
        />
      </HomeItem>
    </>
  );
};

export default NewArrival;
