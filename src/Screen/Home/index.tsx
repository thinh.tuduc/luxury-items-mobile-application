import React from 'react';
// import {View, ScrollView} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import MainHome from './components/MainHome';
import Product from 'src/Screen/Product';

const Stack = createNativeStackNavigator();

const Home = () => {
  return (
    <>
      <Stack.Navigator
        initialRouteName="MainHome"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="MainHome" component={MainHome} />
        <Stack.Screen name="Product" component={Product} />
      </Stack.Navigator>
    </>
  );
};

export default Home;
