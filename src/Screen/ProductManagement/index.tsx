import React, {useState} from 'react';
import {Text, View, TouchableOpacity, TextInput} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

import Table from 'src/components/Table';

const ProductManagement = () => {
  const [productSearch, setProductSearch] = useState<string>('');
  const field = [
    {
      name: 'Category',
      width: 150,
    },
    {
      name: 'Product Name',
      width: 150,
    },
    {
      name: 'Brand',
      width: 150,
    },
    {
      name: 'Added Date',
      width: 150,
    },
    {
      name: 'In Stock',
      width: 100,
    },
  ];
  const width = [150, 150, 150, 150, 100];
  const fieldData = [
    {
      category: 'Womenswear',
      name: 'Ruffled Ribbed cotton Polo Shirt',
      brand: 'Bottega Veneta',
      date: '07/08/2021',
      stock: 1000,
    },
    {
      category: 'Womenswear',
      name: 'Ruffled Ribbed cotton Polo Shirt',
      brand: 'Bottega Veneta',
      date: '07/08/2021',
      stock: 1000,
    },
    {
      category: 'Womenswear',
      name: 'Ruffled Ribbed cotton Polo Shirt',
      brand: 'Bottega Veneta',
      date: '07/08/2021',
      stock: 1000,
    },
    {
      category: 'Womenswear',
      name: 'Ruffled Ribbed cotton Polo Shirt',
      brand: 'Bottega Veneta',
      date: '07/08/2021',
      stock: 1000,
    },
    {
      category: 'Womenswear',
      name: 'Ruffled Ribbed cotton Polo Shirt',
      brand: 'Bottega Veneta',
      date: '07/08/2021',
      stock: 1000,
    },
    {
      category: 'Womenswear',
      name: 'Ruffled Ribbed cotton Polo Shirt',
      brand: 'Bottega Veneta',
      date: '07/08/2021',
      stock: 1000,
    },
  ];
  return (
    <>
      <View style={styles.container}>
        <Flex justify="between" style={styles.space_bot}>
          <Text style={styles.text}>Products</Text>
          <TouchableOpacity style={styles.add_button}>
            <Flex>
              <Icon
                name="plus"
                style={styles.icon}
                size={17 * default_style.width_ratio}
              />
              <Text style={styles.text_add_button}>Add Product</Text>
            </Flex>
          </TouchableOpacity>
        </Flex>
        <Flex justify="between" style={styles.space_bot}>
          <View style={styles.input_container}>
            <Flex align="center">
              <Icon
                name="search"
                size={default_style.icon_size}
                style={styles.icon_input}
              />
              <TextInput
                value={productSearch}
                style={styles.input}
                placeholder="Search for product..."
                onChangeText={val => {
                  setProductSearch(val);
                }}
              />
            </Flex>
          </View>
          <View style={styles.filter_wrapper}>
            <Icon
              name="filter"
              size={default_style.icon_size}
              style={styles.icon_input}
            />
          </View>
        </Flex>
        <Table
          type="product"
          dataWidth={width}
          field={field}
          fieldData={fieldData}
        />
      </View>
    </>
  );
};

export default ProductManagement;
