import React from 'react';
import {Text, View} from 'react-native';

import {styles} from './style';

const Dashboard = () => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text}>This is Dashboard</Text>
      </View>
    </>
  );
};

export default Dashboard;
