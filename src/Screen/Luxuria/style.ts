import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: default_style.white,
  },
  header: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
  },
  text: {
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_600,
    color: default_style.black,
  },
  icon: {
    color: '#777777',
  },
});
