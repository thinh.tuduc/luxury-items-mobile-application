import React, {useState, useRef} from 'react';
import {ImageBackground, View, ScrollView, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

import useInterval from 'src/utils/useInterval';

const Carousel = () => {
  const scrollRef = useRef<any>(null);
  const [currentImg, setCurrentImg] = useState(0);

  useInterval(() => handleAnimation(), 2000);

  const images = [
    'https://i.pinimg.com/originals/d9/56/9d/d9569d5339bbea66c7d0973915d03bca.jpg',
    'https://i.pinimg.com/564x/00/38/d0/0038d0c6640770e5264062ebe4ab49b5.jpg',
    'https://i.pinimg.com/originals/93/2c/3f/932c3fe521fdacf2963f072efb408ca2.jpg',
  ];

  const handleAnimation = () => {
    let newCur = currentImg + 1;
    if (newCur >= images.length) {
      newCur = 0;
    }
    scrollRef.current.scrollTo({
      x: default_style.screen_width * newCur,
      y: 0,
      animated: true,
    });
    setCurrentImg(newCur);
  };
  return (
    <View>
      <ScrollView
        ref={scrollRef}
        scrollEnabled={false}
        pagingEnabled
        horizontal
        showsHorizontalScrollIndicator={false}>
        {images.map((x, id: number) => (
          <ImageBackground
            key={id}
            source={{uri: x}}
            resizeMode="cover"
            style={styles.image}>
            <LinearGradient
              colors={[
                'rgba(34, 34, 34, 0)',
                'rgba(34, 34, 34, 0.67)',
                'rgba(34, 34, 34, 1)',
              ]}
              style={styles.contentWrapper}>
              <Text style={styles.date}>June 18</Text>
              <Text style={styles.title}>
                6 chefs share their most-loved summer recipe
              </Text>
              <Text style={styles.description}>
                You can't reboot the monitor without backing up the haptic GB
                system!
              </Text>
            </LinearGradient>
          </ImageBackground>
        ))}
      </ScrollView>
      <View style={styles.dotWrapper}>
        {images.map((x, id) => (
          <View
            key={id}
            style={[
              styles.dot,
              id === currentImg ? styles.active_dot : undefined,
            ]}
          />
        ))}
      </View>
    </View>
  );
};

export default Carousel;
