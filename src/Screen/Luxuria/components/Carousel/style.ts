import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  image: {
    width: default_style.screen_width,
    height: 510 * default_style.height_ratio,
    position: 'relative',
  },
  dotWrapper: {
    width: default_style.screen_width,
    position: 'absolute',
    flexDirection: 'row',
    left: '45%',
    bottom: 5,
    zIndex: 2,
  },
  dot: {
    width: 9 * default_style.width_ratio,
    height: 9 * default_style.height_ratio,
    borderRadius: 4.5 * default_style.width_ratio,
    backgroundColor: default_style.white,
    marginBottom: 10,
    marginRight: 10,
  },
  active_dot: {
    backgroundColor: default_style.light_sun,
  },
  contentWrapper: {
    width: default_style.screen_width,
    paddingHorizontal: 20,
    paddingTop: 10,
    paddingBottom: 35,
    position: 'absolute',
    bottom: 0,
  },
  date: {
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_normal,
    color: default_style.white,
  },
  title: {
    fontSize: default_style.xxl,
    fontFamily: default_style.font_family_600,
    color: default_style.white,
    lineHeight: default_style.line_height_normal + 4,
  },
  description: {
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    color: default_style.white,
    lineHeight: default_style.line_height_normal,
  },
});
