import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
  },
  header_text: {
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_500,
    color: default_style.black,
    marginBottom: 10,
  },
  card_wrapper: {
    width: 175,
    borderWidth: 1,
    borderColor: '#dcdcdc',
    borderRadius: 4,
    // marginHorizontal: 10,
    marginBottom: 15,
  },
  image_wrapper: {
    width: '100%',
    height: 102,
    backgroundColor: '#eaeaea',
  },
  card_body_wrapper: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  text_date: {
    color: default_style.gray,
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
  },
  text_title: {
    color: default_style.light_black,
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    lineHeight: default_style.line_height_normal,
    marginVertical: 3,
  },
  card_margin: {
    marginLeft: 15,
  },
});
