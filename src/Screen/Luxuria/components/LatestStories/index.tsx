import React from 'react';
import {Text, View} from 'react-native';
import MasonryList from '@react-native-seoul/masonry-list';

import {styles} from './style';

interface CardProp {
  id: number;
  date: string;
  title: string;
}

const LatestStories = () => {
  const Card = ({id, date, title}: CardProp) => (
    <View
      style={
        id % 2 === 0
          ? [styles.card_wrapper, styles.card_margin]
          : styles.card_wrapper
      }>
      <View style={styles.image_wrapper} />
      <View style={styles.card_body_wrapper}>
        <Text style={styles.text_date}>{date}</Text>
        <Text style={styles.text_title}>{title}</Text>
      </View>
    </View>
  );
  const stories = [
    {id: 1, date: 'June 18', title: 'How to apply self-tan like a pro'},
    {
      id: 2,
      date: 'June 18',
      title: '6 chefs share their most-loved summer recipe',
    },
    {
      id: 3,
      date: 'June 18',
      title: '6 chefs share their most-loved summer recipe',
    },
    {id: 4, date: 'June 18', title: 'How to apply self-tan like a pro'},
    {id: 5, date: 'June 18', title: 'How to apply self-tan like a pro'},
    {
      id: 6,
      date: 'June 18',
      title: '6 chefs share their most-loved summer recipe',
    },
    {
      id: 7,
      date: 'June 18',
      title: '6 chefs share their most-loved summer recipe',
    },
    {id: 8, date: 'June 18', title: 'How to apply self-tan like a pro'},
  ];
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.header_text}>Latest Stories</Text>
        <MasonryList
          data={stories}
          numColumns={2}
          renderItem={({item}: any) => (
            <Card id={item.id} date={item.date} title={item.title} />
          )}
        />
      </View>
    </>
  );
};

export default LatestStories;
