import React from 'react';
import {Text, View, ScrollView} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import Carousel from './components/Carousel';
import LatestStories from './components/LatestStories';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const Luxuria = () => {
  return (
    <>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <View style={styles.header}>
          <Flex justify="between">
            <Text style={styles.text}>Luxuria Magazine</Text>
            <Icon
              name="filter"
              size={default_style.icon_size}
              style={styles.icon}
            />
          </Flex>
        </View>
        <Carousel />
        <LatestStories />
      </ScrollView>
    </>
  );
};

export default Luxuria;
