import React from 'react';
import {Text, View} from 'react-native';
// import {Tab, TabView} from 'react-native-elements';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import Products from './components/Products';
import Vendors from './components/Vendors';
import Brands from './components/Brands';
import Editorial from './components/Editorial';

import {styles} from './style';
// import {default_style} from 'src/constant/default_style';

const renderScene = SceneMap({
  0: Products,
  1: Vendors,
  2: Brands,
  3: Editorial,
});

const Favorites = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '0', title: 'Products'},
    {key: '1', title: 'Vendors'},
    {key: '2', title: 'Brands'},
    {key: '3', title: 'Editorial'},
  ]);
  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      renderLabel={({route, focused}) => (
        <Text style={focused ? styles.text_focused : styles.text_unfocused}>
          {route.title}
        </Text>
      )}
      indicatorStyle={styles.indicator}
      tabStyle={styles.tab}
      style={styles.tab_bar}
    />
  );
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text_title}>Favorites</Text>
      </View>
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        renderTabBar={renderTabBar}
      />
    </>
  );
};

export default Favorites;
