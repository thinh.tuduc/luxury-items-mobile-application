import React, {useRef, useState} from 'react';
import {Text, View, TouchableOpacity, ScrollView} from 'react-native';
import {Icon, Flex} from '@ant-design/react-native';
import useInterval from 'src/utils/useInterval';

import Card from 'src/components/Card';
import ProductItem from '../ProductItem';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const data = [
  {
    name: 'Ruffled Ribbed cotton polo shirt',
    oldPrice: '1,200,000 VNĐ',
    newPrice: '1,000,000 VNĐ',
    sale: 25,
  },
  {
    name: 'Ruffled Ribbed cotton polo shirt',
    oldPrice: '1,200,000 VNĐ',
    newPrice: '600,000 VNĐ',
    sale: 50,
  },
  {
    name: 'Ruffled Ribbed cotton polo shirt',
    oldPrice: '1,200,000 VNĐ',
    newPrice: '1,080,000 VNĐ',
    sale: 10,
  },
];

const Vendors = () => {
  const scrollRef = useRef<any>(null);
  const [currentImg, setCurrentImg] = useState(0);

  useInterval(() => handleAnimation(), 3000);

  const handleAnimation = () => {
    let newCur = currentImg + 1;
    if (newCur >= data.length) {
      newCur = 0;
    }
    scrollRef.current.scrollTo({
      x: (default_style.screen_width - 2 * 31) * newCur,
      y: 0,
      animated: true,
    });
    setCurrentImg(newCur);
  };

  const header = (
    <View style={styles.header}>
      <View style={styles.avatar} />
      <Text style={styles.text_header}>Luxury Shop</Text>
      <Icon
        name="right"
        size={16 * default_style.width_ratio}
        style={styles.icon}
      />
    </View>
  );
  const body = (
    <View>
      <ScrollView
        ref={scrollRef}
        scrollEnabled={false}
        pagingEnabled
        horizontal
        showsHorizontalScrollIndicator={false}>
        {data.map((x, id: number) => (
          <View key={id} style={styles.product_wrapper}>
            <ProductItem
              name={x.name}
              oldPrice={x.oldPrice}
              newPrice={x.newPrice}
              sale={x.sale}
            />
          </View>
        ))}
      </ScrollView>
      <Flex justify="center">
        <View style={styles.dotWrapper}>
          {data.map((x, id) => (
            <View
              key={id}
              style={[
                styles.dot,
                id === currentImg ? styles.active_dot : undefined,
              ]}
            />
          ))}
        </View>
      </Flex>
    </View>
  );
  const footer = (
    <TouchableOpacity style={styles.footer_button_unfavorite}>
      <Flex>
        <Icon name="heart" style={styles.icon_unfavorite} />
        <Text style={styles.text_unfavorite}>Unfavorite</Text>
      </Flex>
    </TouchableOpacity>
  );
  return (
    <View style={styles.container}>
      <Card header={header} body={body} footer={footer} />
    </View>
  );
};

export default Vendors;
