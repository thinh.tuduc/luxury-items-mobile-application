import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 20 * default_style.height_ratio,
    backgroundColor: default_style.white,
    height: '100%',
  },
  header: {
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    width: 16 * default_style.width_ratio,
    height: 16 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    marginRight: 5 * default_style.width_ratio,
  },
  text_header: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
    marginRight: 5 * default_style.width_ratio,
  },
  icon: {
    color: '#BEBEBE',
  },
  text: {
    fontSize: 20,
  },
  footer_button_unfavorite: {
    borderWidth: 1,
    borderColor: '#BEBEBE',
    borderRadius: 2 * default_style.width_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20 * default_style.width_ratio,
    paddingVertical: 10 * default_style.height_ratio,
  },
  text_unfavorite: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
  },
  icon_unfavorite: {
    color: '#777777',
    marginRight: 5 * default_style.width_ratio,
  },
  footer_button_cart: {
    backgroundColor: default_style.light_sun,
    borderRadius: 2 * default_style.width_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20 * default_style.width_ratio,
    paddingVertical: 10 * default_style.height_ratio,
  },
  text_cart: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  icon_cart: {
    color: default_style.black,
    marginRight: 5 * default_style.width_ratio,
  },
  product_wrapper: {
    width: default_style.screen_width - 2 * 31,
  },
  dotWrapper: {
    flexDirection: 'row',
    marginTop: 15,
  },
  dot: {
    width: 9,
    height: 9,
    borderRadius: 4.5,
    backgroundColor: '#EAEAEA',
    marginRight: 10,
  },
  active_dot: {
    backgroundColor: default_style.light_sun,
  },
});
