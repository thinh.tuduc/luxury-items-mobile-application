import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Icon, Flex} from '@ant-design/react-native';

import Card from 'src/components/Card';
import ProductItem from '../ProductItem';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const Products = () => {
  const header = (
    <View style={styles.header}>
      <View style={styles.avatar} />
      <Text style={styles.text_header}>Luxury Shop</Text>
      <Icon
        name="right"
        size={16 * default_style.width_ratio}
        style={styles.icon}
      />
    </View>
  );
  const body = (
    <View>
      <ProductItem
        name="Ruffled Ribbed cotton polo shirt"
        oldPrice="1,200,000 VNĐ"
        newPrice="1,000,000 VNĐ"
        sale={25}
      />
    </View>
  );
  const footer = (
    <Flex justify="between" align="center">
      <TouchableOpacity style={styles.footer_button_unfavorite}>
        <Flex>
          <Icon name="heart" style={styles.icon_unfavorite} />
          <Text style={styles.text_unfavorite}>Unfavorite</Text>
        </Flex>
      </TouchableOpacity>
      <TouchableOpacity style={styles.footer_button_cart}>
        <Flex>
          <Icon name="shopping-cart" style={styles.icon_cart} />
          <Text style={styles.text_cart}>Add to Cart</Text>
        </Flex>
      </TouchableOpacity>
    </Flex>
  );
  return (
    <View style={styles.container}>
      <Card header={header} body={body} footer={footer} />
    </View>
  );
};

export default Products;
