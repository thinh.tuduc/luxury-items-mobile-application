import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 20 * default_style.height_ratio,
    backgroundColor: default_style.white,
    height: '100%',
  },
  avatar: {
    width: 80 * default_style.width_ratio,
    height: 80 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    marginRight: 15 * default_style.width_ratio,
  },
  info: {
    width: 250 * default_style.width_ratio,
  },
  text_date: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.gray,
  },
  text_title: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  footer_button_unfavorite: {
    width: 160 * default_style.width_ratio,
    borderWidth: 1,
    borderColor: '#BEBEBE',
    borderRadius: 2 * default_style.width_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20 * default_style.width_ratio,
    paddingVertical: 10 * default_style.height_ratio,
  },
  text_unfavorite: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
  },
  icon_unfavorite: {
    color: '#777777',
    marginRight: 5 * default_style.width_ratio,
  },
});
