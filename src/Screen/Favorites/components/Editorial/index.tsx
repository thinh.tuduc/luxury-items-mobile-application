import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Icon, Flex} from '@ant-design/react-native';

import Card from 'src/components/Card';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const Editorial = () => {
  const body = (
    <View>
      <Flex justify="start" align="center">
        <View style={styles.avatar} />
        <Flex direction="column" align="start" style={styles.info}>
          <Text style={styles.text_date}>June 18</Text>
          <Text style={styles.text_title} numberOfLines={2}>
            6 chefs share their most-loved recipe
          </Text>
        </Flex>
      </Flex>
    </View>
  );
  const footer = (
    <Flex justify="between" align="center">
      <TouchableOpacity style={styles.footer_button_unfavorite}>
        <Flex>
          <Icon
            name="heart"
            style={styles.icon_unfavorite}
            size={default_style.icon_size}
          />
          <Text style={styles.text_unfavorite}>Unfavorite</Text>
        </Flex>
      </TouchableOpacity>
      <TouchableOpacity style={styles.footer_button_unfavorite}>
        <Flex>
          <Icon
            name="share-alt"
            style={styles.icon_unfavorite}
            size={default_style.icon_size}
          />
          <Text style={styles.text_unfavorite}>Share</Text>
        </Flex>
      </TouchableOpacity>
    </Flex>
  );
  return (
    <View style={styles.container}>
      <Card body={body} footer={footer} />
    </View>
  );
};

export default Editorial;
