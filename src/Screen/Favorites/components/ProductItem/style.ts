import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: default_style.white,
    height: '100%',
  },
  avatar: {
    width: 80 * default_style.width_ratio,
    height: 80 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
  },
  text_name: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_old_price: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: 20 * default_style.height_ratio,
    color: '#BEBEBE',
    textDecorationLine: 'line-through',
  },
  text_new_price: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
  },
});
