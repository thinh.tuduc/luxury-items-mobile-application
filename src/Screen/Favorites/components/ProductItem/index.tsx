import React from 'react';
import {Text, View} from 'react-native';
import {Flex} from '@ant-design/react-native';

import Tag from 'src/components/Tag';

import {styles} from './style';

interface ProductItemProp {
  name: string;
  oldPrice: string;
  newPrice: string;
  sale: number;
}

const ProductItem = ({name, oldPrice, newPrice, sale}: ProductItemProp) => {
  return (
    <Flex justify="between" align="center">
      <View style={styles.avatar} />
      <Flex direction="column" align="end">
        <Text style={styles.text_name}>{name}</Text>
        <Text style={styles.text_old_price}>{oldPrice}</Text>
        <Flex justify="end" align="center">
          <Tag content={`${sale}% off`} />
          <Text style={styles.text_new_price}>{newPrice}</Text>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default ProductItem;
