import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  header: {
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    // height: '100%',
  },
  body: {
    backgroundColor: default_style.white,
    height: '100%',
  },
  text_header: {
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_600,
    color: default_style.black,
    lineHeight: default_style.line_height_normal,
  },
  option_wrapper: {
    marginVertical: 20 * default_style.height_ratio,
    marginHorizontal: default_style.standard_padding_horizontal,
  },
  text_option: {
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  text_value: {
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    color: default_style.gray,
    lineHeight: default_style.line_height_normal,
  },
  icon: {
    color: '#777777',
    marginLeft: 10 * default_style.width_ratio,
  },
  divider: {
    height: 1 * default_style.height_ratio,
    backgroundColor: '#BEBEBE',
    marginVertical: 10 * default_style.height_ratio,
    marginHorizontal: default_style.standard_padding_horizontal,
  },
});
