import React from 'react';
import {Text, View, TouchableHighlight} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const AppSetting = ({navigation}: any) => {
  return (
    <>
      <View style={styles.header}>
        <Text style={styles.text_header}>App Settings</Text>
      </View>
      <View style={styles.body}>
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor="#f6f6f6"
          onPress={() => navigation.navigate('Notification')}>
          <Flex justify="between" style={styles.option_wrapper}>
            <Text style={styles.text_option}>Notification</Text>
            <Flex justify="between">
              <Text style={styles.text_value}>Show all</Text>
              <Icon
                size={20 * default_style.width_ratio}
                name="arrow-right"
                style={styles.icon}
              />
            </Flex>
          </Flex>
        </TouchableHighlight>
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor="#f6f6f6"
          onPress={() => navigation.navigate('Language')}>
          <Flex justify="between" style={styles.option_wrapper}>
            <Text style={styles.text_option}>Language</Text>
            <Flex justify="between">
              <Text style={styles.text_value}>English</Text>
              <Icon
                size={20 * default_style.width_ratio}
                name="arrow-right"
                style={styles.icon}
              />
            </Flex>
          </Flex>
        </TouchableHighlight>
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor="#f6f6f6"
          onPress={() => navigation.navigate('Currency')}>
          <Flex justify="between" style={styles.option_wrapper}>
            <Text style={styles.text_option}>Currency</Text>
            <Flex justify="between">
              <Text style={styles.text_value}>VNĐ</Text>
              <Icon
                size={20 * default_style.width_ratio}
                name="arrow-right"
                style={styles.icon}
              />
            </Flex>
          </Flex>
        </TouchableHighlight>
        <View style={styles.divider} />
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor="#f6f6f6"
          onPress={() => navigation.navigate('Help_CustomerService')}>
          <Flex justify="between" style={styles.option_wrapper}>
            <Text style={styles.text_option}>Help & Customer Service</Text>
            <Flex justify="between">
              <Icon
                size={20 * default_style.width_ratio}
                name="arrow-right"
                style={styles.icon}
              />
            </Flex>
          </Flex>
        </TouchableHighlight>
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor="#f6f6f6"
          onPress={() => navigation.navigate('Terms_Conditions')}>
          <Flex justify="between" style={styles.option_wrapper}>
            <Text style={styles.text_option}>Terms And Conditions</Text>
            <Flex justify="between">
              <Icon
                size={20 * default_style.width_ratio}
                name="arrow-right"
                style={styles.icon}
              />
            </Flex>
          </Flex>
        </TouchableHighlight>
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor="#f6f6f6"
          onPress={() => navigation.navigate('Policy')}>
          <Flex justify="between" style={styles.option_wrapper}>
            <Text style={styles.text_option}>Privacy Policy</Text>
            <Flex justify="between">
              <Icon
                size={20 * default_style.width_ratio}
                name="arrow-right"
                style={styles.icon}
              />
            </Flex>
          </Flex>
        </TouchableHighlight>
        <Flex justify="between" style={styles.option_wrapper}>
          <Text style={styles.text_option}>App Version</Text>
          <Flex justify="between">
            <Text style={styles.text_value}>v 0.0.1</Text>
          </Flex>
        </Flex>
      </View>
    </>
  );
};

export default AppSetting;
