import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const Currency = ({navigation}: any) => {
  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Flex align="center">
            <Icon
              name="arrow-left"
              size={20 * default_style.width_ratio}
              style={styles.icon}
            />
            <Text style={styles.text}>Currency</Text>
          </Flex>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default Currency;
