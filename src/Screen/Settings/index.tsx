import React from 'react';
// import {Text, View} from 'react-native';
// import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import AppSetting from './components/App_Setting';
import Notification from './components/Notification';
import Language from './components/Language';
import Currency from './components/Currency';
import Help_CustomerService from './components/Help_CustomerService';
import Terms_Conditions from './components/Terms_Conditions';
import Policy from './components/Policy';

// import {styles} from './style';

const Stack = createNativeStackNavigator();

const Settings = () => {
  return (
    <Stack.Navigator
      initialRouteName="AppSetting"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="App Settings" component={AppSetting} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Language" component={Language} />
      <Stack.Screen name="Currency" component={Currency} />
      <Stack.Screen
        name="Help_CustomerService"
        component={Help_CustomerService}
      />
      <Stack.Screen name="Terms_Conditions" component={Terms_Conditions} />
      <Stack.Screen name="Policy" component={Policy} />
    </Stack.Navigator>
  );
};

export default Settings;
