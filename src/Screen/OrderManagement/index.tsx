import React, {useState} from 'react';
import {Text, View, TextInput, ScrollView} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

import Table from 'src/components/Table';

const OrderManagement = () => {
  const [orderSearch, setOrderSearch] = useState<string>('');
  const field = [
    {
      name: 'Order Info',
      width: 190,
    },
    {
      name: 'Date',
      width: 150,
    },
    {
      name: 'Recipient',
      width: 150,
    },
    {
      name: 'Total Amount',
      width: 150,
    },
  ];
  const width = [190, 150, 150, 150];
  const fieldData = [
    {
      orderName: '12092021CodySimons',
      orderID: '20583956',
      date: '07/08/2021',
      time: '12:20 AM',
      recipient: 'Cody Simons',
      totalAmount: '10,000,000 VNĐ',
      status: 'Processing',
    },
    {
      orderName: '12092021CodySimons',
      orderID: '20583956',
      date: '07/08/2021',
      time: '12:20 AM',
      recipient: 'Cody Simons',
      totalAmount: '10,000,000 VNĐ',
      status: 'Packed',
    },
    {
      orderName: '12092021CodySimons',
      orderID: '20583956',
      date: '07/08/2021',
      time: '12:20 AM',
      recipient: 'Cody Simons',
      totalAmount: '10,000,000 VNĐ',
      status: 'Shipping',
    },
    {
      orderName: '12092021CodySimons',
      orderID: '20583956',
      date: '07/08/2021',
      time: '12:20 AM',
      recipient: 'Cody Simons',
      totalAmount: '10,000,000 VNĐ',
      status: 'Completed',
    },
    {
      orderName: '12092021CodySimons',
      orderID: '20583956',
      date: '07/08/2021',
      time: '12:20 AM',
      recipient: 'Cody Simons',
      totalAmount: '10,000,000 VNĐ',
      status: 'Canceled',
    },
  ];
  const status = ['Processing', 'Packed', 'Shipping', 'Completed', 'Canceled'];
  return (
    <>
      <View style={styles.container}>
        <Flex justify="between" style={styles.space_bot}>
          <Text style={styles.text}>Orders</Text>
        </Flex>
        <Flex justify="between" style={styles.space_bot}>
          <View style={styles.input_container}>
            <Flex align="center">
              <Icon
                name="search"
                size={default_style.icon_size}
                style={styles.icon_input}
              />
              <TextInput
                value={orderSearch}
                style={styles.input}
                placeholder="Search for order or recipient..."
                onChangeText={val => {
                  setOrderSearch(val);
                }}
              />
            </Flex>
          </View>
          <View style={styles.filter_wrapper}>
            <Icon
              name="filter"
              size={default_style.icon_size}
              style={styles.icon_input}
            />
          </View>
        </Flex>
        <Flex justify="between" style={styles.space_bot}>
          <ScrollView
            style={styles.status_container}
            horizontal
            showsHorizontalScrollIndicator={false}>
            {status.map((x, id: number) => {
              return (
                <View key={id} style={styles.status_wrapper}>
                  <Text style={styles.text_status}>{x}</Text>
                </View>
              );
            })}
          </ScrollView>
        </Flex>
        <Table
          type="order"
          dataWidth={width}
          field={field}
          fieldData={fieldData}
        />
      </View>
    </>
  );
};

export default OrderManagement;
