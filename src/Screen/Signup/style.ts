import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: '100%',
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  top_wrapper: {
    // flex: 1,
  },
  header: {
    flexDirection: 'row',
    marginBottom: 25 * default_style.height_ratio,
  },
  back_icon: {
    color: default_style.black,
    marginRight: 10 * default_style.width_ratio,
  },
  text_title: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  text_welcome: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.xxl,
    lineHeight: 28 * default_style.height_ratio,
    color: default_style.light_black,
    marginRight: 10 * default_style.width_ratio,
  },
  text_field: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
    marginBottom: 3 * default_style.height_ratio,
  },
  input_wrapper: {
    marginTop: 12 * default_style.height_ratio,
  },
  bot5: {
    marginBottom: 5 * default_style.height_ratio,
  },
  input_container: {
    borderWidth: 1,
    borderColor: '#BEBEBE',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 4 * default_style.width_ratio,
    paddingHorizontal: 8,
  },
  icon_input: {
    color: '#777777',
  },
  input: {
    padding: 5,
    width: 320 * default_style.width_ratio,
  },
  prefix: {
    flexDirection: 'row',
  },
  text_linking: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: 22 * default_style.height_ratio,
    color: '#2F54EB',
    textDecorationLine: 'underline',
  },
  button_wrapper: {
    backgroundColor: default_style.light_sun,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 12 * default_style.height_ratio,
    marginTop: 20 * default_style.height_ratio,
    marginBottom: 10 * default_style.height_ratio,
    borderRadius: 4 * default_style.width_ratio,
  },
  text_button: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  have_account_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text_have_account: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_error: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: '#ec1414',
    marginBottom: 10 * default_style.height_ratio,
  },
  text_privacy: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
    textAlign: 'center',
    marginBottom: 10 * default_style.height_ratio,
  },
  picker_wrapper: {
    width: 135 * default_style.width_ratio,
    height: 40 * default_style.height_ratio,
    borderWidth: 1,
    borderColor: '#BEBEBE',
    justifyContent: 'center',
    borderRadius: 4 * default_style.width_ratio,
  },
  text_picker: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.5,
    backgroundColor: '#222222',
  },
});
