import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {Icon, Flex} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

import {signup} from 'src/service/identity';

const Signup = (props: any) => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [reEnterPassword, setReEnterPassword] = useState<string>('');
  const [role, setRole] = useState<string>('buyer');
  const [hidden, setHidden] = useState<boolean>(true);
  const [reEnterHidden, setReEnterHidden] = useState<boolean>(true);
  const [errorSignup, setErrorSignup] = useState<any>('');
  const [loading, setLoading] = useState<boolean>(false);

  const handleSignup = () => {
    if (email.length === 0 && password.length === 0) {
      setLoading(true);
      setTimeout(() => {
        setErrorSignup('Email & Password cannot be empty');
        setLoading(false);
      }, 1000);
    } else if (email.length === 0 && password.length > 0) {
      setLoading(true);
      setTimeout(() => {
        setErrorSignup('Email cannot be empty');
        setLoading(false);
      }, 1000);
    } else if (email.length > 0 && password.length === 0) {
      setLoading(true);
      setTimeout(() => {
        setErrorSignup('Password cannot be empty');
        setLoading(false);
      }, 1000);
    } else {
      if (password !== reEnterPassword) {
        setLoading(true);
        setTimeout(() => {
          setErrorSignup('Password does not match');
          setLoading(false);
        }, 1000);
      } else {
        setLoading(true);
        (async () => {
          try {
            const signupData = await signup(email, password, role);
            if (signupData.status === 200) {
              setLoading(false);
              props.navigation.navigate('Login');
            }
          } catch (error) {
            console.log(error);
            setErrorSignup(error);
            setLoading(false);
          }
        })();
      }
    }
  };

  return (
    <SafeAreaView>
      <KeyboardAvoidingView behavior="height" keyboardVerticalOffset={-100}>
        <View
          pointerEvents={loading ? 'none' : 'auto'}
          style={styles.container}>
          <View style={styles.top_wrapper}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              activeOpacity={0.6}
              style={styles.header}>
              <Icon
                name="arrow-left"
                size={default_style.icon_size}
                style={styles.back_icon}
              />
              <Text style={styles.text_title}>Sign Up</Text>
            </TouchableOpacity>
            <Flex align="center">
              <Text style={styles.text_welcome}>Sign Up as a</Text>
              <View style={styles.picker_wrapper}>
                <Picker
                  // mode="dropdown"
                  selectedValue={role}
                  onValueChange={(itemValue: any) => setRole(itemValue)}>
                  <Picker.Item
                    label="Buyer"
                    value="buyer"
                    style={styles.text_picker}
                  />
                  <Picker.Item
                    label="Vendor"
                    value="vendor"
                    style={styles.text_picker}
                  />
                </Picker>
              </View>
            </Flex>
            <View style={styles.input_wrapper}>
              <Text style={styles.text_field}>Email Address</Text>
              <View style={styles.input_container}>
                <Flex align="center">
                  <Icon
                    name="user"
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                  <TextInput
                    value={email}
                    style={styles.input}
                    placeholder="Email Address"
                    onChangeText={val => {
                      setEmail(val);
                    }}
                  />
                </Flex>
              </View>
            </View>
            <View style={styles.input_wrapper}>
              <Text style={styles.text_field}>Password</Text>
              <View style={styles.input_container}>
                <Flex align="center">
                  <Icon
                    name="lock"
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                  <TextInput
                    value={password}
                    style={styles.input}
                    secureTextEntry={hidden}
                    placeholder="Password"
                    onChangeText={val => {
                      setPassword(val);
                    }}
                  />
                </Flex>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => setHidden(!hidden)}>
                  <Icon
                    name={hidden ? 'eye-invisible' : 'eye'}
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.input_wrapper}>
              <Text style={styles.text_field}>Re-Enter Password</Text>
              <View style={styles.input_container}>
                <Flex align="center">
                  <Icon
                    name="lock"
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                  <TextInput
                    value={reEnterPassword}
                    style={styles.input}
                    secureTextEntry={reEnterHidden}
                    placeholder="Re-Enter Password"
                    onChangeText={val => {
                      setReEnterPassword(val);
                    }}
                  />
                </Flex>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => setReEnterHidden(!reEnterHidden)}>
                  <Icon
                    name={reEnterHidden ? 'eye-invisible' : 'eye'}
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={handleSignup}
              style={styles.button_wrapper}>
              <Text style={styles.text_button}>Sign Up</Text>
            </TouchableOpacity>
            <View style={styles.have_account_wrapper}>
              {errorSignup ? (
                <Text style={styles.text_error}>{errorSignup}</Text>
              ) : null}
              <Text style={styles.text_have_account}>
                Already have an account?
              </Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => props.navigation.navigate('Login')}>
                <Text style={styles.text_linking}>Log in</Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.text_privacy}>
            By Signing Up, you agree to Luxury Item’s{' '}
            <Text style={styles.text_linking}>Term of Service</Text> &{' '}
            <Text style={styles.text_linking}>Privacy Policy</Text>
          </Text>
        </View>
        {loading && (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color="#ffffff" />
          </View>
        )}
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Signup;
