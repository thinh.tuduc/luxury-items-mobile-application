import React from 'react';
import {Text, View, Image} from 'react-native';

import {styles} from './style';

const OngoingOrder = ({userSession}: any) => {
  return (
    <>
      {userSession.isLogin ? (
        <View style={styles.container}>
          <Text>This is On-going Order</Text>
        </View>
      ) : (
        <View style={styles.img_wrapper}>
          <Image resizeMode="cover" source={require('src/img/NoData.png')} />
          <Text style={styles.text}>Seems like there’s nothing here</Text>
        </View>
      )}
    </>
  );
};

export default OngoingOrder;
