import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 20 * default_style.height_ratio,
    backgroundColor: default_style.white,
    height: '100%',
  },
  text: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.gray,
    marginTop: 10 * default_style.height_ratio,
  },
  img_wrapper: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});
