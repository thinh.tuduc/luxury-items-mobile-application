import React from 'react';
import {Text, View} from 'react-native';
import {Flex} from '@ant-design/react-native';

import {styles} from './style';

interface ProductItemProp {
  name: string;
  price: string;
  quantity: number;
}

const OrderItem = ({name, price, quantity}: ProductItemProp) => {
  return (
    <Flex justify="between" align="start">
      <View style={styles.avatar} />
      <Flex direction="column" align="stretch">
        <Text style={styles.text_name}>{name}</Text>
        <Flex justify="between" align="center">
          <Text style={styles.text_price}>{`Qty: ${quantity}`}</Text>
          <Text style={styles.text_price}>{price}</Text>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default OrderItem;
