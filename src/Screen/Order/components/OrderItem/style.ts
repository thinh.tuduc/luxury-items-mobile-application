import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: default_style.white,
    height: '100%',
  },
  avatar: {
    width: 80 * default_style.width_ratio,
    height: 80 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    // marginRight: 20 * default_style.height_ratio,
  },
  text_name: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_price: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: '#9E9E9E',
  },
});
