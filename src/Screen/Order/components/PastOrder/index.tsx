import React from 'react';
import {Text, View, Image} from 'react-native';

import {styles} from './style';

const PastOrder = ({userSession}: any) => {
  return (
    <>
      {userSession.isLogin ? (
        <View style={styles.container}>
          <Text>This is Past Order</Text>
        </View>
      ) : (
        <View style={styles.img_wrapper}>
          <Image resizeMode="cover" source={require('src/img/NoData.png')} />
          <Text style={styles.text}>Seems like there’s nothing here</Text>
        </View>
      )}
    </>
  );
};

export default PastOrder;
