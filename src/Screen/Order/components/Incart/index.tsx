import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Icon, Flex} from '@ant-design/react-native';

import Card from 'src/components/Card';
import OrderItem from '../OrderItem';

import Tag from 'src/components/Tag';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const Incart = () => {
  const header = (
    <Flex justify="between" align="center">
      <Flex align="center">
        <View style={styles.avatar} />
        <Text style={styles.text_header}>Luxury Shop</Text>
        <Icon
          name="right"
          size={16 * default_style.width_ratio}
          style={styles.icon}
        />
      </Flex>
      <Tag content="In cart" />
    </Flex>
  );
  const body = (
    <>
      <View style={styles.bottom_spacing}>
        <OrderItem
          name="Ruffled Ribbed cotton polo shirt"
          price="1,200,000 VNĐ"
          quantity={1}
        />
      </View>
      <View>
        <OrderItem
          name="Ruffled Ribbed cotton polo shirt"
          price="1,200,000 VNĐ"
          quantity={1}
        />
      </View>
    </>
  );

  const footer = (
    <Flex justify="between" align="center">
      <Text style={styles.text_total}>Total:</Text>
      <Text style={styles.text_price_total}>2,400,000 VNĐ</Text>
    </Flex>
  );

  const extraFooter = (
    <TouchableOpacity style={styles.footer_button_cart}>
      <Flex>
        <Icon name="shopping-cart" style={styles.icon_cart} />
        <Text style={styles.text_cart}>Add to Cart</Text>
      </Flex>
    </TouchableOpacity>
  );
  return (
    <View style={styles.container}>
      <Card
        header={header}
        body={body}
        footer={footer}
        extraFooter={extraFooter}
      />
    </View>
  );
};

export default Incart;
