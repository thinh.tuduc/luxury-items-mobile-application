import React from 'react';
import {Text, View} from 'react-native';
// import {Tab, TabView} from 'react-native-elements';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import Incart from './components/Incart';
import OngoingOrder from './components/OngoingOrder';
import PastOrder from './components/PastOrder';

import {styles} from './style';
// import {default_style} from 'src/constant/default_style';

const Order = (props: any) => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '0', title: 'In Cart (1)'},
    {key: '1', title: 'On-going Orders'},
    {key: '2', title: 'Past Orders'},
  ]);
  const renderScene = SceneMap({
    0: Incart,
    1: () => <OngoingOrder userSession={props.userSession} />,
    2: () => <PastOrder userSession={props.userSession} />,
  });
  const renderTabBar = (tabBarprops: any) => (
    <TabBar
      {...tabBarprops}
      renderLabel={({route, focused}) => (
        <Text style={focused ? styles.text_focused : styles.text_unfocused}>
          {route.title}
        </Text>
      )}
      indicatorStyle={styles.indicator}
      tabStyle={styles.tab}
      style={styles.tab_bar}
    />
  );
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text_title}>Orders</Text>
      </View>
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        renderTabBar={renderTabBar}
      />
    </>
  );
};

export default Order;
