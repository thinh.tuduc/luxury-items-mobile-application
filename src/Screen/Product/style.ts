import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBF9F8',
  },
  footer: {
    width: default_style.screen_width,
    height: 65 * default_style.height_ratio,
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    justifyContent: 'center',
  },
  shadow: {
    width: default_style.screen_width,
    height: 1 * default_style.height_ratio,
    opacity: 0.5,
    zIndex: 100,
  },
  icon_wrapper: {
    width: 40 * default_style.width_ratio,
    height: 40 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EAEAEA',
    borderRadius: 2 * default_style.width_ratio,
  },
  divider: {
    width: 1 * default_style.width_ratio,
    height: 40 * default_style.height_ratio,
    backgroundColor: '#DCDCDC',
    marginHorizontal: 10 * default_style.width_ratio,
  },
  button_wrapper: {
    width:
      (default_style.screen_width -
        (2 * 15 + 40 + 1 + 2 * 10 + 10) * default_style.width_ratio) /
      2,
    height: 40 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2 * default_style.width_ratio,
  },
  add_background: {
    backgroundColor: '#EAEAEA',
    marginRight: 10 * default_style.width_ratio,
  },
  buy_background: {
    backgroundColor: default_style.light_sun,
  },
  text_button: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: default_style.black,
    lineHeight: default_style.line_height_normal,
  },
  to_top_wrapper: {
    position: 'absolute',
    right: '4%',
    top: '82%',
  },
  to_top_button: {
    width: 40 * default_style.width_ratio,
    height: 40 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(85, 85, 85, 0.4)',
    marginBottom: 10 * default_style.height_ratio,
    borderRadius: 4,
  },
  icon: {
    color: default_style.white,
  },
  icon_message: {
    color: default_style.light_black,
  },
});
