import React, {useState, useRef} from 'react';
import {View, ScrollView, Text, TouchableOpacity} from 'react-native';

import {Flex, Icon} from '@ant-design/react-native';
import LinearGradient from 'react-native-linear-gradient';

import ProductInfo from './components/ProductInfo';
import Coupon from './components/Coupon';
import Variation from './components/Variation';
import ProductDetail from './components/ProductDetail';
import Review from './components/Review';
import ShopInfo from './components/ShopInfo';
import Recommend from './components/Recommend';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const Product = (props: any) => {
  const scrollRef = useRef<any>(null);
  const [offsetY, setOffsetY] = useState<number>(0);

  return (
    <>
      <View style={styles.container}>
        <ScrollView
          ref={scrollRef}
          onScroll={e => setOffsetY(e.nativeEvent.contentOffset.y)}
          showsVerticalScrollIndicator={false}>
          <ProductInfo goBack={() => props.navigation.goBack()} />
          <Coupon />
          <Variation />
          <ProductDetail />
          <Review />
          <ShopInfo />
          <Recommend {...props} />
        </ScrollView>
        {offsetY >= 750 && (
          <TouchableOpacity
            onPress={() =>
              scrollRef.current.scrollTo({
                x: 0,
                y: 0,
                animated: true,
              })
            }
            style={styles.to_top_wrapper}>
            <View style={styles.to_top_button}>
              <Icon
                name="arrow-up"
                size={default_style.icon_size}
                style={styles.icon}
              />
            </View>
          </TouchableOpacity>
        )}
        <View>
          <LinearGradient
            colors={[
              'transparent',
              'rgba(34, 34, 34, 0.05)',
              'rgba(34, 34, 34, 0.1)',
            ]}
            style={styles.shadow}
          />
          <View style={styles.footer}>
            <Flex>
              <View style={styles.icon_wrapper}>
                <Icon
                  name="message"
                  size={default_style.icon_size}
                  style={styles.icon_message}
                />
              </View>
              <View style={styles.divider} />
              <TouchableOpacity
                style={[styles.button_wrapper, styles.add_background]}>
                <Text style={styles.text_button}>Add to Cart</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button_wrapper, styles.buy_background]}>
                <Text style={styles.text_button}>Buy Now</Text>
              </TouchableOpacity>
            </Flex>
          </View>
        </View>
      </View>
    </>
  );
};

export default Product;
