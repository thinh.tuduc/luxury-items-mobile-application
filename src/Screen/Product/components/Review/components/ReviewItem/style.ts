import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  space_bot: {
    marginBottom: 15 * default_style.height_ratio,
  },
  space_bot_8: {
    marginBottom: 8 * default_style.height_ratio,
  },
  avatar: {
    width: 24 * default_style.width_ratio,
    height: 24 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    borderRadius: 12 * default_style.width_ratio,
    marginRight: 10 * default_style.width_ratio,
  },
  text_username: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.md,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  text_date: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    color: default_style.gray,
    lineHeight: default_style.line_height_20,
    marginLeft: 10 * default_style.width_ratio,
  },
  divider: {
    width: 1 * default_style.width_ratio,
    height: 20 * default_style.height_ratio,
    backgroundColor: '#DCDCDC',
    marginLeft: 10 * default_style.width_ratio,
  },
  text_content: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
});
