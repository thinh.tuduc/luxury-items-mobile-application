import React from 'react';
import {View, Text} from 'react-native';
import {Flex} from '@ant-design/react-native';

import Rating from 'src/components/Rating';

import {styles} from './style';
// import {default_style} from 'src/constant/default_style';

interface ReviewProp {
  content: string;
  date: string;
  username: string;
  rating: number;
}

const ReviewItem = ({content, date, username, rating}: ReviewProp) => {
  return (
    <Flex direction="column" align="start" style={styles.space_bot}>
      <Flex style={styles.space_bot_8} align="center">
        <View style={styles.avatar} />
        <Text style={styles.text_username}>{username}</Text>
      </Flex>
      <Flex align="center" style={styles.space_bot}>
        <Rating
          size={15}
          disabled={false}
          defaultValue={rating}
          numberStar={[1, 2, 3, 4, 5]}
          onChange={() => {}}
        />
        <Text style={styles.text_date}>{date}</Text>
        <View style={styles.divider} />
        <Text style={styles.text_date}>Color: Black</Text>
      </Flex>
      <Text style={styles.text_content}>{content}</Text>
    </Flex>
  );
};

export default ReviewItem;
