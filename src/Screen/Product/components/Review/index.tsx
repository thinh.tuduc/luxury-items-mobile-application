import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import Rating from 'src/components/Rating';
import ReviewItem from './components/ReviewItem';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const data = [
  {
    username: 'User1',
    rating: 4,
    date: '07/08/2021',
    content:
      'The SAS monitor is down, back up the virtual panel so we can hack the SDD matrix!',
  },
  {
    username: 'User2',
    rating: 3,
    date: '07/08/2021',
    content:
      'The SAS monitor is down, back up the virtual panel so we can hack the SDD matrix!',
  },
];

const Review = () => {
  return (
    <View style={styles.container}>
      <Flex justify="between" align="center" style={styles.space_bot}>
        <Text style={styles.text_title}>Reviews (153)</Text>
        <Icon
          name="arrow-right"
          size={default_style.icon_size}
          style={styles.icon}
        />
      </Flex>
      <Flex justify="between" align="center">
        <Flex style={styles.general_wrapper} direction="column" align="center">
          <Text style={styles.text_total_rating_score}>4.0</Text>
          <Rating
            size={15}
            disabled={true}
            defaultValue={4}
            numberStar={[1, 2, 3, 4, 5]}
            onChange={() => {}}
          />
          <Text style={styles.text_total_rating}>153 ratings</Text>
        </Flex>
        <Flex style={styles.space_bot_10} direction="column" align="center">
          <Flex style={styles.space_bot_10} justify="between" align="center">
            <Rating
              size={15}
              disabled={true}
              defaultValue={5}
              numberStar={[1, 2, 3, 4, 5]}
              onChange={() => {}}
            />
            <View style={styles.line_rate} />
            <Text style={styles.text_line_rate}>05</Text>
          </Flex>
          <Flex style={styles.space_bot_10} justify="between" align="center">
            <Rating
              size={15}
              disabled={true}
              defaultValue={4}
              numberStar={[1, 2, 3, 4, 5]}
              onChange={() => {}}
            />
            <View style={styles.line_rate} />
            <Text style={styles.text_line_rate}>999+</Text>
          </Flex>
          <Flex style={styles.space_bot_10} justify="between" align="center">
            <Rating
              size={15}
              disabled={true}
              defaultValue={3}
              numberStar={[1, 2, 3, 4, 5]}
              onChange={() => {}}
            />
            <View style={styles.line_rate} />
            <Text style={styles.text_line_rate}>129</Text>
          </Flex>
          <Flex style={styles.space_bot_10} justify="between" align="center">
            <Rating
              size={15}
              disabled={true}
              defaultValue={2}
              numberStar={[1, 2, 3, 4, 5]}
              onChange={() => {}}
            />
            <View style={styles.line_rate} />
            <Text style={styles.text_line_rate}>01</Text>
          </Flex>
          <Flex style={styles.space_bot_10} justify="between" align="center">
            <Rating
              size={15}
              disabled={true}
              defaultValue={1}
              numberStar={[1, 2, 3, 4, 5]}
              onChange={() => {}}
            />
            <View style={styles.line_rate} />
            <Text style={styles.text_line_rate}>00</Text>
          </Flex>
        </Flex>
      </Flex>
      {data.map((x: any, id: number) => {
        return (
          <ReviewItem
            key={id}
            username={x.username}
            rating={x.rating}
            date={x.date}
            content={x.content}
          />
        );
      })}
      <TouchableOpacity style={styles.button} activeOpacity={0.5}>
        <Text style={styles.text_button}>See All Reviews</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Review;
