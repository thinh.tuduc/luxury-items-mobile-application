import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_title: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  icon: {
    color: '#777777',
  },
  space_bot: {
    marginBottom: 20 * default_style.height_ratio,
  },
  space_bot_10: {
    marginBottom: 10 * default_style.height_ratio,
  },
  general_wrapper: {
    // paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 10 * default_style.height_ratio,
    // marginRight: 30 * default_style.width_ratio,
  },
  text_total_rating_score: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.fz38,
    color: default_style.light_black,
    lineHeight: default_style.line_height_48,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_total_rating: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
    marginTop: 10 * default_style.height_ratio,
  },
  line_rate: {
    width: 145 * default_style.width_ratio,
    height: 6 * default_style.height_ratio,
    backgroundColor: '#C4C4C4',
    borderRadius: 4,
    marginHorizontal: 8 * default_style.width_ratio,
  },
  text_line_rate: {
    width: 36 * default_style.width_ratio,
    textAlign: 'left',
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  button: {
    width: '100%',
    height: 40 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#BEBEBE',
  },
  text_button: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: '#777777',
    lineHeight: default_style.line_height_normal,
  },
});
