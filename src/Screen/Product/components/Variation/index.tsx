import React from 'react';
import {View, ScrollView, Text} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const data = [{url: 'abcd'}, {url: 'abcd'}, {url: 'abcd'}, {url: 'abcd'}];

const Variation = () => {
  return (
    <View style={styles.container}>
      <Flex justify="between" align="center" style={styles.space_bot}>
        <Text style={styles.text_title}>Variation (4)</Text>
        <Icon
          name="arrow-right"
          size={default_style.icon_size}
          style={styles.icon}
        />
      </Flex>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {data.map((x: any, id: number) => (
          <View
            key={id}
            style={
              id === data.length - 1
                ? styles.image
                : [styles.image, styles.spacing]
            }
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default Variation;
