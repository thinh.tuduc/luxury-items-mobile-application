import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const ShopInfo = () => {
  return (
    <View style={styles.container}>
      <Flex align="center">
        <View style={styles.avatar} />
        <Flex direction="column" align="start">
          <Flex align="center" justify="center" style={styles.space_bot_2}>
            <Text style={styles.text_shop_name}>The Fashion Shop</Text>
            <Icon
              name="right"
              size={17 * default_style.width_ratio}
              style={[styles.icon, styles.space_top_2]}
            />
          </Flex>
          <Text style={styles.text_time}>Active 3 hours ago</Text>
        </Flex>
      </Flex>
      <Flex justify="between" align="center" style={styles.statistic_wrapper}>
        <Flex direction="column" align="center">
          <Text style={styles.text_number}>4.9/5</Text>
          <Text style={styles.text_field}>Rating</Text>
        </Flex>
        <Flex direction="column" align="center">
          <Text style={styles.text_number}>978</Text>
          <Text style={styles.text_field}>Followers</Text>
        </Flex>
        <Flex direction="column" align="center">
          <Text style={styles.text_number}>90%</Text>
          <Text style={styles.text_field}>Response</Text>
        </Flex>
      </Flex>
      <TouchableOpacity style={styles.button} activeOpacity={0.5}>
        <Flex align="center">
          <Icon
            name="heart"
            size={default_style.icon_size}
            style={[styles.icon, styles.spacing]}
          />
          <Text style={styles.text_button}>Follow Store</Text>
        </Flex>
      </TouchableOpacity>
    </View>
  );
};

export default ShopInfo;
