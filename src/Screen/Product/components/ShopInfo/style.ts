import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
    marginBottom: 10 * default_style.height_ratio,
  },
  space_bot: {
    marginBottom: 20 * default_style.height_ratio,
  },
  space_bot_10: {
    marginBottom: 10 * default_style.height_ratio,
  },
  space_bot_2: {
    marginBottom: 2 * default_style.height_ratio,
  },
  space_top_2: {
    marginTop: 2 * default_style.height_ratio,
  },
  spacing: {
    marginRight: 10 * default_style.width_ratio,
  },
  avatar: {
    width: 48 * default_style.width_ratio,
    height: 48 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    borderRadius: 2 * default_style.width_ratio,
    marginRight: 10 * default_style.width_ratio,
  },
  text_shop_name: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.md,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
    marginRight: 5 * default_style.width_ratio,
  },
  icon: {
    color: '#777777',
  },
  text_time: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.gray,
    lineHeight: default_style.line_height_normal,
  },
  statistic_wrapper: {
    marginHorizontal: 25 * default_style.width_ratio,
    marginVertical: 20 * default_style.height_ratio,
  },
  text_number: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  text_field: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    color: default_style.gray,
    lineHeight: default_style.line_height_20,
  },
  button: {
    width: '100%',
    height: 40 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#BEBEBE',
  },
  text_button: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: '#777777',
    lineHeight: default_style.line_height_normal,
  },
});
