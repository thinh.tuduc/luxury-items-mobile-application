import React from 'react';
import {View, ScrollView, Text} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const data = [
  {
    title: 'Free Shipping',
    description: 'Send đ49,000 get đ20,000 shipping fee off 13-15 Aug, 2021',
  },
  {
    title: '10% off',
    description: 'Send đ49,000 get đ20,000 shipping fee off 13-15 Aug, 2021',
  },
];

interface CouponCardProp {
  title: string;
  description: string;
  isLastItem: boolean;
}

const Coupon = () => {
  const CouponCard = ({title, description, isLastItem}: CouponCardProp) => (
    <View
      style={
        isLastItem
          ? styles.coupon_card_wrapper
          : [styles.coupon_card_wrapper, styles.coupon_card_spacing]
      }>
      <Text style={[styles.text_title, styles.space_text]}>{title}</Text>
      <Text style={styles.text_coupon_card_description}>{description}</Text>
    </View>
  );
  return (
    <View style={styles.container}>
      <Flex justify="between" align="center" style={styles.space_bot}>
        <Text style={styles.text_title}>Coupon</Text>
        <Icon
          name="arrow-right"
          size={default_style.icon_size}
          style={styles.icon}
        />
      </Flex>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {data.map((x: any, id: number) => (
          <CouponCard
            key={id}
            title={x.title}
            description={x.description}
            isLastItem={id === data.length - 1}
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default Coupon;
