import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_title: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  icon: {
    color: '#777777',
  },
  space_bot: {
    marginBottom: 20 * default_style.height_ratio,
  },
  space_text: {
    marginBottom: 10 * default_style.height_ratio,
  },
  coupon_card_wrapper: {
    width: 250 * default_style.width_ratio,
    paddingHorizontal: 15 * default_style.width_ratio,
    paddingVertical: 10 * default_style.height_ratio,
    backgroundColor: '#F6F6F6',
  },
  coupon_card_spacing: {
    marginRight: 10 * default_style.width_ratio,
  },
  text_coupon_card_description: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
});
