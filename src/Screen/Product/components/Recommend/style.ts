import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_title: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  icon: {
    color: '#777777',
  },
  space_bot: {
    marginBottom: 20 * default_style.height_ratio,
  },
  card_wrapper: {
    width: 175,
    borderWidth: 1,
    borderColor: '#dcdcdc',
    borderRadius: 4,
    // marginHorizontal: 10,
    marginBottom: 15,
  },
  image_wrapper: {
    width: '100%',
    height: 102,
    backgroundColor: '#eaeaea',
  },
  card_body_wrapper: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  text_name: {
    color: default_style.light_black,
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    lineHeight: default_style.line_height_normal,
  },
  text_branch: {
    color: default_style.gray,
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    lineHeight: default_style.line_height_normal,
    marginVertical: 3,
  },
  text_price: {
    color: default_style.light_black,
    fontSize: default_style.md,
    fontFamily: default_style.font_family_500,
    lineHeight: default_style.line_height_normal,
  },
  card_margin: {
    marginLeft: 15,
  },
});
