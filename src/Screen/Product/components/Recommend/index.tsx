import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';
import MasonryList from '@react-native-seoul/masonry-list';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

interface CardProp {
  id: number;
  name: string;
  branch: string;
  price: string;
}

const Recommend = (props: any) => {
  const Card = ({id, name, branch, price}: CardProp) => (
    <View
      style={
        id % 2 === 0
          ? [styles.card_wrapper, styles.card_margin]
          : styles.card_wrapper
      }>
      <View style={styles.image_wrapper} />
      <View style={styles.card_body_wrapper}>
        <Text style={styles.text_name}>{name}</Text>
        <Text style={styles.text_branch}>{branch}</Text>
        <Text style={styles.text_price}>{price}</Text>
      </View>
    </View>
  );
  const arrival = [
    {
      id: 1,
      name: 'Ruffled ribbed cotton polo shirt',
      branch: 'Bottega Veneta',
      price: '1,200,000 VNĐ',
    },
    {
      id: 2,
      name: '+ NET SUSTAIN printed organic cotton polo shirt',
      branch: 'Stella McCartney',
      price: '2,290,000 VNĐ',
    },
    {
      id: 3,
      name: 'Embroidered cotton-jersey T-shirt',
      branch: 'Balenciaga',
      price: '3,750,000 VNĐ',
    },
    {id: 4, name: 'Printed T-shirt', branch: 'Chloe', price: '6,790,000 VNĐ'},
    {id: 5, name: 'Leather Slippers', branch: 'Vans', price: '1,200,000 VNĐ'},
    {
      id: 6,
      name: 'Oversized Woven Blazer - Red',
      branch: 'Versace',
      price: '1,200,000 VNĐ',
    },
    {
      id: 7,
      name: 'Oversized Woven Blazer - Red',
      branch: 'Versace',
      price: '1,200,000 VNĐ',
    },
    {id: 8, name: 'Leather Slippers', branch: 'Vans', price: '1,200,000 VNĐ'},
  ];
  return (
    <>
      <View style={styles.container}>
        <Flex justify="between" align="center" style={styles.space_bot}>
          <Text style={styles.text_title}>Recommendation</Text>
          <Icon
            name="arrow-right"
            size={default_style.icon_size}
            style={styles.icon}
          />
        </Flex>
        <MasonryList
          data={arrival}
          numColumns={2}
          renderItem={({item, i}: any) => (
            <TouchableOpacity
              key={i}
              onPress={() => props.navigation.push('Product')}>
              <Card
                id={item.id}
                name={item.name}
                branch={item.branch}
                price={item.price}
              />
            </TouchableOpacity>
          )}
        />
      </View>
    </>
  );
};

export default Recommend;
