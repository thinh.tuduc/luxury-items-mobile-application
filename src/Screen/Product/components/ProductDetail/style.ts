import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_title: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  icon: {
    color: '#777777',
  },
  space_bot: {
    marginBottom: 20 * default_style.height_ratio,
  },
  text_field: {
    width: 100 * default_style.width_ratio,
    fontFamily: default_style.font_family_500,
    fontSize: default_style.md,
    color: default_style.gray,
    lineHeight: default_style.line_height_normal,
  },
  text_field_inside: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: '#2F54EB',
    lineHeight: default_style.line_height_normal,
    textDecorationLine: 'underline',
  },
  breadcrumb: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.gray,
    lineHeight: default_style.line_height_normal,
    marginHorizontal: 10 * default_style.width_ratio,
  },
  text_detail: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  divider: {
    width: '100%',
    height: 2 * default_style.height_ratio,
    backgroundColor: '#DCDCDC',
  },
  space_text: {
    marginVertical: 15 * default_style.height_ratio,
  },
  button: {
    width: '100%',
    height: 40 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#BEBEBE',
  },
  text_button: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: '#777777',
    lineHeight: default_style.line_height_normal,
  },
});
