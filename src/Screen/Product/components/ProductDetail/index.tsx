import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const ProductDetail = () => {
  const [isMore, setMore] = useState<boolean>(false);
  return (
    <View style={styles.container}>
      <Flex justify="between" align="center" style={styles.space_bot}>
        <Text style={styles.text_title}>Product Details</Text>
        <Icon
          name="arrow-right"
          size={default_style.icon_size}
          style={styles.icon}
        />
      </Flex>
      <Flex align="center" style={styles.space_bot}>
        <Text style={styles.text_field}>Category</Text>
        <Text style={styles.text_field_inside}>Womenswear</Text>
        <Text style={styles.breadcrumb}>/</Text>
        <Text style={styles.text_field_inside}>Dresses</Text>
      </Flex>
      <Flex align="center" style={styles.space_bot}>
        <Text style={styles.text_field}>Brand</Text>
        <Text style={styles.text_field_inside}>Bottega Venetta</Text>
      </Flex>
      <Flex align="center" style={styles.space_bot}>
        <Text style={styles.text_field}>Material</Text>
        <Text style={styles.text_detail}>Cotton</Text>
      </Flex>
      <View style={styles.divider} />
      <Text
        style={[styles.text_detail, styles.space_text]}
        numberOfLines={isMore ? 0 : 2}>
        Real Avocado Nutrition Oil Serum 100ml. A serum enriched with avocado
        oil, rich in vitamin whenever lorem ipsum lorem ipsum lorem ipsum lorem
        ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem
        ipsum lorem ipsum lorem ipsum
      </Text>
      <TouchableOpacity
        style={styles.button}
        activeOpacity={0.5}
        onPress={() => setMore(!isMore)}>
        <Text style={styles.text_button}>{`Read ${
          isMore ? 'Less' : 'Full'
        } Description`}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ProductDetail;
