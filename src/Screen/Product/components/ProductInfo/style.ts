import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    marginBottom: 10 * default_style.height_ratio,
  },
  carousel: {
    position: 'relative',
  },
  image: {
    width: default_style.screen_width,
    height: 210 * default_style.height_ratio,
  },
  go_back: {
    width: 26 * default_style.width_ratio,
    height: 26 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: default_style.light_black,
    borderRadius: 13 * default_style.width_ratio,
    position: 'absolute',
    top: 10 * default_style.height_ratio,
    left: 10 * default_style.width_ratio,
  },
  dotWrapper: {
    width: default_style.screen_width,
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'center',
    bottom: 0,
    zIndex: 2,
  },
  dot: {
    width: 9 * default_style.width_ratio,
    height: 9 * default_style.height_ratio,
    borderRadius: 4.5 * default_style.width_ratio,
    backgroundColor: default_style.white,
    marginBottom: 10 * default_style.height_ratio,
    marginRight: 10 * default_style.width_ratio,
  },
  active_dot: {
    backgroundColor: default_style.light_sun,
  },
  product_wrapper: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
  },
  text_new_price: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.xxl,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  icon_wrapper: {
    width: 40 * default_style.width_ratio,
    height: 40 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#BEBEBE',
  },
  icon: {
    color: default_style.light_black,
  },
  space_icon: {
    marginRight: 10 * default_style.width_ratio,
  },
  text_old_price: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    color: default_style.gray,
    lineHeight: default_style.line_height_normal,
    textDecorationLine: 'line-through',
    marginRight: 10 * default_style.width_ratio,
  },
  text_product_name: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  space_bot: {
    marginBottom: 20 * default_style.height_ratio,
  },
  divider: {
    width: 1 * default_style.width_ratio,
    height: 24 * default_style.height_ratio,
    backgroundColor: '#DCDCDC',
    marginLeft: 10 * default_style.width_ratio,
  },
  text_spec: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
    marginLeft: 10 * default_style.width_ratio,
  },
});
