import React, {useRef, useState} from 'react';
import {Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';
// import {Rating} from 'react-native-ratings';
import Rating from 'src/components/Rating';

// import useInterval from 'src/utils/useInterval';
import Tag from 'src/components/Tag';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const images = [
  'https://api.time.com/wp-content/uploads/2017/01/cute-animal-tweet-off-zoo.jpg',
  'https://nustudentlife.files.wordpress.com/2012/04/siberian_husky_2.jpg',
  'https://static.boredpanda.com/blog/wp-content/uploads/2017/04/cute-dog-shiba-inu-ryuji-japan-57.jpg',
];

interface ProductProp {
  goBack: () => {};
}

const ProductInfo = ({goBack}: ProductProp) => {
  const scrollRef = useRef<any>(null);
  const [currentImg, setCurrentImg] = useState(0);

  const handleAnimation = (offset: any) => {
    let index = Math.floor(offset.x) / Math.floor(default_style.screen_width);
    if (index % 1 === 0) {
      setCurrentImg(index);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <ScrollView
          ref={scrollRef}
          scrollEnabled={true}
          pagingEnabled
          horizontal
          style={styles.carousel}
          onScroll={e => handleAnimation(e.nativeEvent.contentOffset)}
          showsHorizontalScrollIndicator={false}>
          {images.map((x, id: number) => (
            <Image
              key={id}
              source={{uri: x}}
              resizeMode="cover"
              style={styles.image}
            />
          ))}
        </ScrollView>
        <TouchableOpacity onPress={goBack} style={styles.go_back}>
          <Icon name="left" size={default_style.icon_size} />
        </TouchableOpacity>
        <View style={styles.dotWrapper}>
          {images.map((x, id) => (
            <View
              key={id}
              style={[
                styles.dot,
                id === currentImg ? styles.active_dot : undefined,
              ]}
            />
          ))}
        </View>
      </View>
      <View style={styles.product_wrapper}>
        <Flex justify="between" align="center">
          <Text style={styles.text_new_price}>1,200,000 VNĐ</Text>
          <Flex justify="between" align="center">
            <View style={[styles.icon_wrapper, styles.space_icon]}>
              <Icon
                name="heart"
                style={styles.icon}
                size={default_style.icon_size}
              />
            </View>
            <View style={styles.icon_wrapper}>
              <Icon
                name="share-alt"
                style={styles.icon}
                size={default_style.icon_size}
              />
            </View>
          </Flex>
        </Flex>
        <Flex style={styles.space_bot}>
          <Text style={styles.text_old_price}> 5,400,000 VNĐ</Text>
          <Tag content="25% off" />
        </Flex>
        <View style={styles.space_bot}>
          <Text style={styles.text_product_name}>
            + NET SUSTAIN printed organic cotton polo shirt
          </Text>
        </View>
        <Flex align="center">
          <Rating
            size={30}
            disabled={false}
            defaultValue={4}
            numberStar={[1, 2, 3, 4, 5]}
            onChange={value => console.log(value)}
          />
          <Text style={styles.text_spec}>1 review</Text>
          <View style={styles.divider} />
          <Text style={styles.text_spec}>4 sold</Text>
        </Flex>
      </View>
    </View>
  );
};

export default ProductInfo;
