import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import {Icon, Flex} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

import {login} from 'src/service/identity';

import EncryptedStorage from 'react-native-encrypted-storage';

const Login = (props: any) => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [hidden, setHidden] = useState<boolean>(true);
  const [loading, setLoading] = useState<boolean>(false);
  const [errorLogin, setErrorLogin] = useState<any>('');

  const handleLogin = () => {
    if (email.length > 0 && password.length === 0) {
      setLoading(true);
      setTimeout(() => {
        setErrorLogin('Password cannot be empty');
        setLoading(false);
      }, 1000);
    } else if (email.length === 0 && password.length > 0) {
      setLoading(true);
      setTimeout(() => {
        setErrorLogin('Email cannot be empty');
        setLoading(false);
      }, 1000);
    } else if (email.length === 0 && password.length === 0) {
      setLoading(true);
      setTimeout(() => {
        setErrorLogin('Email & Password cannot be empty');
        setLoading(false);
      }, 1000);
    } else {
      setLoading(true);
      (async () => {
        try {
          const loginData = await login(email, password);
          if (loginData.status === 200) {
            setLoading(false);
            setEmail('');
            setPassword('');
            await EncryptedStorage.setItem('token', loginData.data.token);
            await EncryptedStorage.setItem('email', loginData.data.email);
            await EncryptedStorage.setItem('role', loginData.data.role);
            const tokenStorage = await EncryptedStorage.getItem('token');
            const emailStorage = await EncryptedStorage.getItem('email');
            const roleStorage = await EncryptedStorage.getItem('role');
            props.dispatch({
              type: 'UPDATE_USER_SESSION',
              payload: {
                userSession: {
                  token: tokenStorage,
                  email: emailStorage,
                  role: roleStorage,
                },
                isLogin: true,
              },
            });
            if (roleStorage === 'vendor') {
              props.navigation.navigate('Dashboard');
            } else {
              props.navigation.navigate('Home');
            }
          }
        } catch (error) {
          console.log(error);
          setErrorLogin(error);
          setLoading(false);
        }
      })();
    }
  };

  return (
    <SafeAreaView>
      <KeyboardAvoidingView behavior="height" keyboardVerticalOffset={-100}>
        <View
          pointerEvents={loading ? 'none' : 'auto'}
          style={styles.container}>
          <View style={styles.top_wrapper}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              activeOpacity={0.6}
              style={styles.header}>
              <Icon
                name="arrow-left"
                size={default_style.icon_size}
                style={styles.back_icon}
              />
              <Text style={styles.text_title}>Log In</Text>
            </TouchableOpacity>
            <Text style={styles.text_welcome}>Welcome to Luxury Items!</Text>
            <View style={styles.input_wrapper}>
              <Text style={styles.text_field}>Email Address</Text>
              <View style={styles.input_container}>
                <Flex align="center">
                  <Icon
                    name="user"
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                  <TextInput
                    value={email}
                    style={styles.input}
                    placeholder="Email Address"
                    onChangeText={val => {
                      setEmail(val);
                    }}
                  />
                </Flex>
              </View>
            </View>
            <View style={[styles.input_wrapper, styles.bot5]}>
              <Text style={styles.text_field}>Password</Text>
              <View style={styles.input_container}>
                <Flex align="center">
                  <Icon
                    name="lock"
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                  <TextInput
                    value={password}
                    style={styles.input}
                    secureTextEntry={hidden}
                    placeholder="Password"
                    onChangeText={val => {
                      setPassword(val);
                    }}
                  />
                </Flex>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => setHidden(!hidden)}>
                  <Icon
                    name={hidden ? 'eye-invisible' : 'eye'}
                    size={default_style.icon_size}
                    style={styles.icon_input}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <Text style={styles.text_linking}>Forgot Password?</Text>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.button_wrapper}
              onPress={handleLogin}>
              <Text style={styles.text_button}>Log In</Text>
            </TouchableOpacity>
            <View style={styles.have_account_wrapper}>
              {errorLogin ? (
                <Text style={styles.text_error}>{errorLogin}</Text>
              ) : null}
              <Text style={styles.text_have_account}>
                Don't have an account yet?
              </Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => props.navigation.navigate('Signup')}>
                <Text style={styles.text_linking}>Sign up</Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.text_privacy}>
            By Loging In, you agree to Luxury Item’s{' '}
            <Text style={styles.text_linking}>Term of Service</Text> &{' '}
            <Text style={styles.text_linking}>Privacy Policy</Text>
          </Text>
        </View>
        {loading && (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color="#ffffff" />
          </View>
        )}
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Login;
