import React from 'react';
import {Text, View} from 'react-native';

import {styles} from './style';

const Report = () => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text}>This is Report</Text>
      </View>
    </>
  );
};

export default Report;
