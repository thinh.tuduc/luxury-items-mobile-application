import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 56 * default_style.height_ratio,
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 10 * default_style.height_ratio,
  },
  avatar: {
    width: 40 * default_style.width_ratio,
    height: 40 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    marginRight: 8 * default_style.width_ratio,
  },
  left15: {
    marginLeft: 15 * default_style.width_ratio,
  },
  text_name: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_time: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_20,
    color: '#BEBEBE',
  },
  icon: {
    color: default_style.black,
  },
  chat_wrapper: {
    backgroundColor: '#FBF9F8',
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 10 * default_style.height_ratio,
  },
  time_wrapper: {
    marginBottom: 5 * default_style.height_ratio,
  },
  message_wrapper: {
    marginVertical: 10 * default_style.height_ratio,
  },
  align_right: {
    textAlign: 'right',
  },
  message_content_wrapper: {
    maxWidth: 240 * default_style.width_ratio,
    paddingHorizontal: 10 * default_style.width_ratio,
    paddingVertical: 8 * default_style.height_ratio,
    backgroundColor: default_style.white,
    borderRadius: 4 * default_style.width_ratio,
  },
  spacing: {
    marginVertical: 10 * default_style.height_ratio,
  },
  text_message: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  send_icon: {
    color: default_style.light_sun,
  },
  footer: {
    width: '100%',
    height: 56 * default_style.height_ratio,
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 8 * default_style.height_ratio,
  },
  input_wrapper: {
    width: 340 * default_style.width_ratio,
    backgroundColor: '#F6F6F6',
    borderRadius: 4 * default_style.width_ratio,
    paddingHorizontal: 8,
  },
  input: {
    paddingVertical: 8 * default_style.height_ratio,
    paddingHorizontal: 8 * default_style.width_ratio,
  },
});
