import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';

const ChatDetail = (props: any) => {
  return (
    <>
      <Flex style={styles.header} justify="between">
        <Flex>
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => props.navigation.navigate('Chat')}>
            <Icon name="left" style={styles.icon} />
          </TouchableOpacity>
          <Flex>
            <View style={[styles.avatar, styles.left15]} />
            <Flex direction="column" align="start">
              <Text style={styles.text_name}>Luxury Shop</Text>
              <Text style={styles.text_time}>Active 5 minutes ago</Text>
            </Flex>
          </Flex>
        </Flex>
        <Icon name="ellipsis" style={styles.icon} />
      </Flex>
      <ScrollView style={styles.chat_wrapper}>
        <Flex style={styles.time_wrapper} justify="center">
          <Text style={styles.text_time}>16:04 05/06/2021</Text>
        </Flex>
        <Flex style={styles.message_wrapper} justify="start">
          <View style={styles.avatar} />
          <View style={styles.message_content_wrapper}>
            <Text style={styles.text_message}>Hi how may I help you?</Text>
          </View>
        </Flex>
        <Flex style={styles.message_wrapper} justify="end">
          <View style={styles.message_content_wrapper}>
            <Text style={styles.text_message}>
              Can you give me more information about this product
            </Text>
          </View>
        </Flex>
        <Flex style={styles.time_wrapper} justify="center">
          <Text style={styles.text_time}>16:04 05/06/2021</Text>
        </Flex>
        <Flex style={styles.message_wrapper} justify="start" align="start">
          <View style={styles.avatar} />
          <View>
            <View style={styles.message_content_wrapper}>
              <Text style={styles.text_message}>
                Okay, I will check the product status.
              </Text>
            </View>
            <View style={[styles.message_content_wrapper, styles.spacing]}>
              <Text style={styles.text_message}>
                Hi, your order is ready and is being picked up by the shipping
                unit and will arrive as soon as possible.
              </Text>
            </View>
          </View>
        </Flex>
        <Flex style={styles.time_wrapper} justify="center">
          <Text style={styles.text_time}>16:04 05/06/2021</Text>
        </Flex>
        <Flex style={styles.message_wrapper} justify="start" align="start">
          <View style={styles.avatar} />
          <View>
            <View style={styles.message_content_wrapper}>
              <Text style={styles.text_message}>
                Okay, I will check the product status.
              </Text>
            </View>
            <View style={[styles.message_content_wrapper, styles.spacing]}>
              <Text style={styles.text_message}>
                Hi, your order is ready and is being picked up by the shipping
                unit and will arrive as soon as possible.
              </Text>
            </View>
          </View>
        </Flex>
        <Flex style={styles.time_wrapper} justify="center">
          <Text style={styles.text_time}>16:04 05/06/2021</Text>
        </Flex>
        <Flex style={styles.message_wrapper} justify="start" align="start">
          <View style={styles.avatar} />
          <View>
            <View style={styles.message_content_wrapper}>
              <Text style={styles.text_message}>
                Okay, I will check the product status.
              </Text>
            </View>
            <View style={[styles.message_content_wrapper, styles.spacing]}>
              <Text style={styles.text_message}>
                Hi, your order is ready and is being picked up by the shipping
                unit and will arrive as soon as possible.
              </Text>
            </View>
          </View>
        </Flex>
      </ScrollView>
      <View style={styles.footer}>
        <Flex justify="between">
          <View style={styles.input_wrapper}>
            <TextInput
              style={styles.input}
              placeholder="Type your message here"
            />
          </View>
          <Icon name="send" style={styles.send_icon} />
        </Flex>
      </View>
    </>
  );
};

export default ChatDetail;
