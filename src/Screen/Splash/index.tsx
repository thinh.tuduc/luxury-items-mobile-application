import React from 'react';
import {View, Image} from 'react-native';

import {styles} from './style';

const Splash = () => {
  return (
    <>
      <View style={styles.container}>
        <Image resizeMode="cover" source={require('src/img/Splash_Logo.png')} />
      </View>
    </>
  );
};

export default Splash;
