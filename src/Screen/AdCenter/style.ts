import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: '100%',
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
  },
  text_title: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  space_bot: {
    marginBottom: 15 * default_style.height_ratio,
  },
  space_bot_7: {
    marginBottom: 7 * default_style.height_ratio,
  },
  space_bot_10: {
    marginBottom: 10 * default_style.height_ratio,
  },
  plan_card_wrapper: {
    borderWidth: 1,
    borderColor: '#E4C39A',
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 25 * default_style.height_ratio,
    marginVertical: 15 * default_style.height_ratio,
  },
  type_text: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.xxl,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_price: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.gray,
    marginTop: 5 * default_style.height_ratio,
  },
  text_price_num: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.fz38,
    lineHeight: default_style.line_height_48,
    color: default_style.black,
  },
  text_description: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
    marginBottom: 20 * default_style.height_ratio,
  },
  text_list_benefit: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  icon_wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 16 * default_style.width_ratio,
    height: 16 * default_style.width_ratio,
    borderRadius: 8 * default_style.width_ratio,
    backgroundColor: '#BEBEBE',
    marginRight: 8 * default_style.width_ratio,
  },
  check_icon: {
    color: default_style.white,
  },
  button_compare: {
    borderWidth: 1,
    borderColor: '#BEBEBE',
    backgroundColor: '#F8F0E6',
    width: '100%',
    height: 60 * default_style.height_ratio,
    marginTop: 20 * default_style.height_ratio,
    marginBottom: 10 * default_style.height_ratio,
  },
  text_compare: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
  },
  button_choose: {
    backgroundColor: default_style.light_sun,
    width: '100%',
    height: 60 * default_style.height_ratio,
  },
  text_choose: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  gold: {
    backgroundColor: '#F8F0E6',
  },
});
