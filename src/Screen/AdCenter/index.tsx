import React from 'react';
import {Text, View, ScrollView} from 'react-native';
import {Flex} from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import {styles} from './style';

const AdCenter = () => {
  const PlanCard = ({type}: any) => {
    return (
      <View
        style={
          type !== 'Gold'
            ? [styles.plan_card_wrapper]
            : [styles.plan_card_wrapper, styles.gold]
        }>
        <Text style={styles.type_text}>
          {type === 'Gold' ? 'Gold (Best Value)' : type}
        </Text>
        <Flex align="start" style={styles.space_bot_10}>
          <Text style={styles.text_price_num}>$12</Text>
          <Text style={styles.text_price}>/month</Text>
        </Flex>
        <Text style={styles.text_description}>
          Billed annually or $15 month-to-month
        </Text>
        <Flex style={styles.space_bot_7}>
          <View style={styles.icon_wrapper}>
            <Icon name="check" style={styles.check_icon} />
          </View>
          <Text style={styles.text_list_benefit}>Benefit 1</Text>
        </Flex>
        <Flex style={styles.space_bot_7}>
          <View style={styles.icon_wrapper}>
            <Icon name="check" style={styles.check_icon} />
          </View>
          <Text style={styles.text_list_benefit}>Benefit 2</Text>
        </Flex>
        <Flex style={styles.space_bot_7}>
          <View style={styles.icon_wrapper}>
            <Icon name="check" style={styles.check_icon} />
          </View>
          <Text style={styles.text_list_benefit}>Benefit 3</Text>
        </Flex>
        <Flex style={styles.space_bot_7}>
          <View style={styles.icon_wrapper}>
            <Icon name="check" style={styles.check_icon} />
          </View>
          <Text style={styles.text_list_benefit}>Benefit 4</Text>
        </Flex>
        <Flex justify="center" style={styles.button_compare}>
          <Text style={styles.text_compare}>Compare Plans</Text>
        </Flex>
        <Flex justify="center" style={styles.button_choose}>
          <Text style={styles.text_choose}>Choose Plans</Text>
        </Flex>
      </View>
    );
  };
  return (
    <>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <Text style={styles.text_title}>Ad Center</Text>
          <PlanCard type="Gold" />
          <PlanCard type="Silver" />
          <PlanCard type="Platinum" />
          <PlanCard type="Titanium" />
        </View>
      </ScrollView>
    </>
  );
};

export default AdCenter;
