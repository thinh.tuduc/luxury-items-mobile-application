import React, {useState} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import {Flex} from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const data = [
  {
    icon: 'skin',
    name: 'Menswear',
  },
  {
    icon: 'skin',
    name: 'Womenswear',
  },
  {
    icon: 'skin',
    name: 'Perfumes',
  },
  {
    icon: 'skin',
    name: 'Accessories',
  },
  {
    icon: 'skin',
    name: 'Arts',
  },
  {
    icon: 'skin',
    name: 'Cars',
  },
  {
    icon: 'skin',
    name: 'Yacht',
  },
  {
    icon: 'skin',
    name: 'Real Estates',
  },
  {
    icon: 'skin',
    name: 'Interior',
  },
  {
    icon: 'skin',
    name: 'Wine',
  },
  {
    icon: 'skin',
    name: 'Gifts',
  },
];

const data2 = [
  {
    name: 'Dresses',
  },
  {
    name: 'Tops',
  },
  {
    name: 'Knitwears',
  },
  {
    name: 'Jackets',
  },
  {
    name: 'Pants',
  },
  {
    name: 'Blouses',
  },
  {
    name: 'Skirts',
  },
  {
    name: 'Pants',
  },
  {
    name: 'Blouses',
  },
  {
    name: 'Skirts',
  },
];

interface CategoryCardProp {
  name: string;
  isLast: boolean;
}

const Categories = () => {
  const [categoryItem, setCategoryItem] = useState<number>(0);

  const CategoryCard = ({name, isLast}: CategoryCardProp) => (
    <View
      style={
        isLast
          ? styles.category_card_wrapper
          : [styles.category_card_wrapper, styles.element_margin_right]
      }>
      <View style={styles.category_img} />
      <Text numberOfLines={1} style={styles.text_category_card_name}>
        {name}
      </Text>
    </View>
  );

  return (
    <>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.text_title}>Categories</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.category_list}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {data.map((x, id: number) => {
                return (
                  <View
                    style={
                      categoryItem !== id
                        ? styles.category_item_wrapper
                        : [styles.category_item_wrapper, styles.active]
                    }
                    key={id}>
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => setCategoryItem(id)}
                      style={styles.category_item}>
                      <Icon
                        name={x.icon}
                        size={default_style.icon_size}
                        style={
                          categoryItem !== id ? styles.icon : styles.icon_active
                        }
                      />
                      <Text
                        style={
                          categoryItem !== id
                            ? styles.text_category_name
                            : styles.text_active
                        }>
                        {x.name}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </ScrollView>
          </View>
          <View style={styles.category_detail}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Flex style={styles.category_detail_wrapper} justify="between">
                <Text style={styles.text_category_detail}>Clothing</Text>
                <Icon
                  name="up"
                  size={default_style.icon_size}
                  style={styles.icon}
                />
              </Flex>
              <View style={styles.element_wrapper}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  <CategoryCard isLast={false} name="Bottega Venetta" />
                  <CategoryCard isLast={false} name="Chloe" />
                  <CategoryCard isLast={false} name="Balenciaga" />
                  <CategoryCard isLast={true} name="Bottega Venetta" />
                </ScrollView>
              </View>
              {data2.map((x, id: number) => {
                return (
                  <View key={id} style={styles.element_wrapper}>
                    <Text style={styles.text_category_detail_name}>
                      {x.name}
                    </Text>
                  </View>
                );
              })}
              <Flex style={styles.category_detail_wrapper} justify="between">
                <Text style={styles.text_category_detail}>Bags</Text>
                <Icon
                  name="down"
                  size={default_style.icon_size}
                  style={styles.icon}
                />
              </Flex>
              <Flex style={styles.category_detail_wrapper} justify="between">
                <Text style={styles.text_category_detail}>Shoes</Text>
                <Icon
                  name="down"
                  size={default_style.icon_size}
                  style={styles.icon}
                />
              </Flex>
            </ScrollView>
          </View>
        </View>
      </View>
    </>
  );
};

export default Categories;
