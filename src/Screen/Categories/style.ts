import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: default_style.white,
    height: '100%',
  },
  header: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
  },
  text_title: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  active: {
    backgroundColor: default_style.white,
    borderBottomWidth: 1,
    borderBottomColor: '#DCDCDC',
    borderLeftWidth: 4,
    borderLeftColor: default_style.light_sun,
  },
  text_active: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_20,
    color: default_style.light_sun,
  },
  icon_active: {
    color: default_style.light_sun,
  },
  body: {
    borderTopWidth: 1,
    borderTopColor: '#DCDCDC',
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
  },
  category_list: {
    backgroundColor: '#F6F6F6',
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: '#F6F6F6',
  },
  category_item_wrapper: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
  },
  category_item: {
    alignItems: 'center',
  },
  icon: {
    color: default_style.gray,
  },
  text_category_name: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_20,
    color: default_style.gray,
  },
  category_detail: {
    flex: 2.5,
  },
  category_detail_wrapper: {
    paddingVertical: 12 * default_style.height_ratio,
    paddingHorizontal: default_style.standard_padding_horizontal,
    borderBottomWidth: 1,
    borderBottomColor: '#DCDCDC',
  },
  text_category_detail: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  category_card_wrapper: {
    width: 100,
    borderWidth: 1,
    borderColor: '#DCDCDC',
    borderRadius: 2,
    paddingHorizontal: 7 * default_style.width_ratio,
    paddingVertical: 7 * default_style.height_ratio,
  },
  category_img: {
    backgroundColor: '#EAEAEA',
    height: 65,
    marginBottom: 7 * default_style.height_ratio,
  },
  text_category_card_name: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_20,
    color: default_style.light_black,
    textAlign: 'center',
  },
  element_wrapper: {
    paddingVertical: default_style.standard_padding_vertical,
    paddingHorizontal: default_style.standard_padding_horizontal,
  },
  text_category_detail_name: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  element_margin_right: {
    marginRight: 10 * default_style.width_ratio,
  },
});
