import React from 'react';
import {Text, View} from 'react-native';
import {Flex} from '@ant-design/react-native';

import {styles} from './style';

const ChatItem = ({hasImg, message, shopName, date, isRead}: any) => {
  return (
    <View style={styles.container}>
      <Flex justify="between" align="center">
        <Flex direction="column" align="start">
          <Flex align="center" justify="start">
            <View style={styles.avatar} />
            <Flex direction="column" align="start">
              {isRead ? (
                <Text style={styles.text_shop_name}>{shopName}</Text>
              ) : (
                <Flex>
                  <Text style={[styles.text_shop_name, styles.bold]}>
                    {shopName}
                  </Text>
                  <View style={styles.dot} />
                </Flex>
              )}
              <Text style={styles.text_date}>{date}</Text>
            </Flex>
          </Flex>
          <Text
            style={
              hasImg
                ? [styles.text_message, styles.text_truncate]
                : styles.text_message
            }
            numberOfLines={1}>
            {message}
          </Text>
        </Flex>
        {hasImg && <View style={styles.img} />}
      </Flex>
    </View>
  );
};

export default ChatItem;
