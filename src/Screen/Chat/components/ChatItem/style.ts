import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15 * default_style.width_ratio,
    paddingVertical: 15 * default_style.height_ratio,
    borderBottomWidth: 1,
    borderBottomColor: '#DCDCDC',
  },
  avatar: {
    width: 45 * default_style.width_ratio,
    height: 45 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    marginRight: 10 * default_style.width_ratio,
  },
  text_shop_name: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
    marginBottom: 2 * default_style.height_ratio,
  },
  text_date: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: 20 * default_style.height_ratio,
    color: '#BEBEBE',
  },
  text_message: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
    marginTop: 10 * default_style.height_ratio,
  },
  img: {
    width: 75 * default_style.width_ratio,
    height: 75 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
  },
  dot: {
    width: 8 * default_style.width_ratio,
    height: 8 * default_style.height_ratio,
    borderRadius: 4 * default_style.width_ratio,
    backgroundColor: default_style.light_sun,
    marginLeft: 10 * default_style.width_ratio,
  },
  bold: {
    fontFamily: default_style.font_family_500,
  },
  text_truncate: {
    width: 245 * default_style.width_ratio,
  },
});
