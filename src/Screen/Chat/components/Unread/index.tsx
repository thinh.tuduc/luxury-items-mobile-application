import React from 'react';
import {View} from 'react-native';

import ChatItem from '../ChatItem';

import {styles} from './style';

const data = [
  {
    shop: 'Luxury Shop',
    date: '05/06/2021',
    first_message: 'Hi, your order is ready and iasda fadaf',
    hasImage: true,
    isRead: false,
  },
  {
    shop: "The Woman's Shop",
    date: '05/06/2021',
    first_message:
      'Welcome to our shop. How may I help you? If you have kjhsd adkjg',
    hasImage: true,
    isRead: false,
  },
  {
    shop: 'Fashion Clothing Shop',
    date: '05/06/2021',
    first_message: 'Your order has been shipped. Any comments you can asda',
    hasImage: true,
    isRead: false,
  },
];

const Unread = () => {
  return (
    <>
      <View style={styles.container}>
        {data.map((x: any, id: number) => {
          return (
            <ChatItem
              key={id}
              hasImg={x.hasImage}
              shopName={x.shop}
              date={x.date}
              message={x.first_message}
              isRead={x.isRead}
            />
          );
        })}
      </View>
    </>
  );
};

export default Unread;
