import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: default_style.white,
    height: '100%',
  },
  text: {
    fontSize: 20,
  },
});
