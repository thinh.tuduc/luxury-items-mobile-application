import React from 'react';
import {Text, View} from 'react-native';
// import {Tab, TabView} from 'react-native-elements';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import All from './components/All';
import Unread from './components/Unread';

import {styles} from './style';
// import {default_style} from 'src/constant/default_style';

const Chat = (props: any) => {
  // const [index, setIndex] = useState(0);
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '0', title: 'All'},
    {key: '1', title: 'Unread'},
  ]);
  const renderScene = SceneMap({
    0: () => <All {...props} />,
    1: () => <Unread />,
  });
  const renderTabBar = (renderProps: any) => (
    <TabBar
      {...renderProps}
      renderLabel={({route, focused}) => (
        <Text style={focused ? styles.text_focused : styles.text_unfocused}>
          {route.title}
        </Text>
      )}
      indicatorStyle={styles.indicator}
      tabStyle={styles.tab}
      style={styles.tab_bar}
    />
  );
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text_title}>Chat</Text>
      </View>
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        renderTabBar={renderTabBar}
      />
    </>
  );
};

export default Chat;
