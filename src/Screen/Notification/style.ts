import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    backgroundColor: default_style.white,
  },
  text_title: {
    fontFamily: default_style.font_family_600,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  text_wrapper: {
    textAlign: 'left',
  },
  text_focused: {
    width: '104%',
    fontFamily: default_style.font_family_500,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
  text_unfocused: {
    width: '104%',
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: '#49485F',
  },
  tab: {
    paddingHorizontal: 15 * default_style.width_ratio,
    width: 'auto',
  },
  tab_bar: {
    backgroundColor: 'white',
  },
  indicator: {
    backgroundColor: default_style.light_black,
    marginHorizontal: 1,
  },
});
