import React from 'react';
import {Text, View} from 'react-native';
import {Flex} from '@ant-design/react-native';

import {styles} from './style';

const Notification_Item = ({
  title,
  date,
  orderID,
  arrivalTime,
  isRead,
}: any) => {
  return (
    <View style={styles.container}>
      <Flex direction="column" align="start">
        <Flex>
          <View style={styles.avatar} />
          <Flex direction="column" align="start">
            {isRead ? (
              <Text style={styles.text_title_read}>{title}</Text>
            ) : (
              <Flex>
                <Text style={styles.text_title}>{title}</Text>
                <View style={styles.dot} />
              </Flex>
            )}
            <Text style={isRead ? styles.text_date_read : styles.text_date}>
              {date}
            </Text>
          </Flex>
        </Flex>
        <View style={styles.noti_wrapper}>
          <Text style={isRead ? styles.text_noti_read : styles.text_noti}>
            Your order <Text style={styles.text_id}>{orderID}</Text> is packed
            and ready for delivery. Estimated time of arrival is before{' '}
            {arrivalTime}
          </Text>
        </View>
      </Flex>
    </View>
  );
};

export default Notification_Item;
