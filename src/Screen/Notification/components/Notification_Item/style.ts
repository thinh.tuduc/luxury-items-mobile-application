import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15 * default_style.width_ratio,
    paddingVertical: 15 * default_style.height_ratio,
    borderBottomWidth: 1,
    borderBottomColor: '#DCDCDC',
  },
  avatar: {
    width: 45 * default_style.width_ratio,
    height: 45 * default_style.height_ratio,
    backgroundColor: '#EAEAEA',
    marginRight: 10 * default_style.width_ratio,
  },
  text_title: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_title_read: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.lg,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_date: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_20,
    color: '#BEBEBE',
  },
  text_date_read: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_20,
    color: '#BEBEBE',
  },
  noti_wrapper: {
    marginTop: 10 * default_style.height_ratio,
  },
  text_noti: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_noti_read: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: '#777777',
  },
  text_id: {
    color: '#2F54EB',
    textDecorationLine: 'underline',
  },
  dot: {
    width: 8 * default_style.width_ratio,
    height: 8 * default_style.height_ratio,
    borderRadius: 4 * default_style.width_ratio,
    backgroundColor: default_style.light_sun,
    marginLeft: 10 * default_style.width_ratio,
  },
});
