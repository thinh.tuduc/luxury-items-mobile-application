import React from 'react';
import {View} from 'react-native';

import Notification_Item from '../Notification_Item';

import {styles} from './style';

const data = [
  {
    title: 'Order ready for delivery',
    date: '05/06/2021',
    orderID: 'VN01506933544865',
    arrivalTime: '23:59 09/05/2021',
    isRead: false,
  },
  {
    title: 'Order delivered',
    date: '05/06/2021',
    orderID: 'VN01506933544865',
    arrivalTime: '23:59 09/05/2021',
    isRead: true,
  },
];

const Orders = () => {
  return (
    <>
      <View style={styles.container}>
        {data.map((x, id: number) => {
          return (
            <Notification_Item
              key={id}
              title={x.title}
              date={x.date}
              orderID={x.orderID}
              arrivalTime={x.arrivalTime}
              isRead={x.isRead}
            />
          );
        })}
      </View>
    </>
  );
};

export default Orders;
