import React from 'react';
import {Text, View} from 'react-native';

import {styles} from './style';

const Promotions = () => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text}>This is Promotions</Text>
      </View>
    </>
  );
};

export default Promotions;
