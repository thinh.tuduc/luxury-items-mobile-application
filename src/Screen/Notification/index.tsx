import React from 'react';
import {Text, View} from 'react-native';
// import {Tab, TabView} from 'react-native-elements';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import Orders from './components/Orders';
import Promotions from './components/Promotions';

import {styles} from './style';
// import {default_style} from 'src/constant/default_style';

const renderScene = SceneMap({
  0: Orders,
  1: Promotions,
});

const Notification = () => {
  // const [index, setIndex] = useState(0);
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: '0', title: 'Orders'},
    {key: '1', title: 'Promotions'},
  ]);
  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      renderLabel={({route, focused}) => (
        <Text style={focused ? styles.text_focused : styles.text_unfocused}>
          {route.title}
        </Text>
      )}
      indicatorStyle={styles.indicator}
      tabStyle={styles.tab}
      style={styles.tab_bar}
    />
  );
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text_title}>Notification</Text>
      </View>
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        renderTabBar={renderTabBar}
      />
    </>
  );
};

export default Notification;
