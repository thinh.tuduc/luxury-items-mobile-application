import React from 'react';
import {Text, View} from 'react-native';

import {styles} from './style';

const Livestream = () => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.text}>This is Livestream</Text>
      </View>
    </>
  );
};

export default Livestream;
