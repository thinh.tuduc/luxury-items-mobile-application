import React from 'react';
// import {Text, View} from 'react-native';
// import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Account_Information from './components/Account_Information';
import Avatar from './components/Avatar';
import Email from './components/Email';
import Mobile_Number from './components/Mobile_Number';
import Fullname from './components/Fullname';
import Change_Password from './components/Change_Password';
import Birthday from './components/Birthday';
import Gender from './components/Gender';

// import {styles} from './style';

const Stack = createNativeStackNavigator();

const Account = (props: any) => {
  return (
    <Stack.Navigator
      initialRouteName="Account_Information"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Account_Information">
        {propsStack => (
          <Account_Information
            userSessionProp={props.userSession}
            dispatch={props.dispatch}
            goBackHome={() => props.navigation.navigate('Home')}
            {...propsStack}
          />
        )}
      </Stack.Screen>
      <Stack.Screen name="Avatar" component={Avatar} />
      <Stack.Screen name="Email" component={Email} />
      <Stack.Screen name="Mobile_Number" component={Mobile_Number} />
      <Stack.Screen name="Fullname" component={Fullname} />
      <Stack.Screen name="Change_Password" component={Change_Password} />
      <Stack.Screen name="Birthday" component={Birthday} />
      <Stack.Screen name="Gender" component={Gender} />
    </Stack.Navigator>
  );
};

export default Account;
