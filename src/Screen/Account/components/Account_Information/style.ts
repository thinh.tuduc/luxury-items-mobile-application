import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  wrapper: {
    flex: 1,
  },
  header: {
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    // height: '100%',
  },
  body: {
    backgroundColor: default_style.white,
    height: '100%',
  },
  text_header: {
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_600,
    color: default_style.black,
    lineHeight: default_style.line_height_normal,
  },
  option_wrapper: {
    marginVertical: 20 * default_style.height_ratio,
    marginHorizontal: default_style.standard_padding_horizontal,
  },
  text_option: {
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    color: default_style.light_black,
    lineHeight: default_style.line_height_normal,
  },
  text_value: {
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    color: default_style.gray,
    lineHeight: default_style.line_height_normal,
  },
  icon: {
    color: '#777777',
    marginLeft: 10 * default_style.width_ratio,
  },
  circle_wrapper: {
    width: 40 * default_style.width_ratio,
    height: 40 * default_style.height_ratio,
    borderRadius: 20 * default_style.width_ratio,
    backgroundColor: '#f9f8f8',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  user_icon: {
    color: default_style.black,
  },
  divider: {
    height: 1 * default_style.height_ratio,
    backgroundColor: '#BEBEBE',
    marginVertical: 10 * default_style.height_ratio,
    marginHorizontal: default_style.standard_padding_horizontal,
  },
  log_out_wrapper: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
  },
  log_out: {
    backgroundColor: default_style.light_sun,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15 * default_style.height_ratio,
  },
  icon_log_out: {
    color: '#272D35',
    marginRight: 5 * default_style.width_ratio,
  },
  text_log_out: {
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_500,
    lineHeight: default_style.line_height_normal,
    color: default_style.black,
  },
});
