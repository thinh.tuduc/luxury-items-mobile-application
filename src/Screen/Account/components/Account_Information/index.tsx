import React from 'react';
import {Text, View, TouchableHighlight} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';
import {TouchableOpacity} from 'react-native-gesture-handler';

// import {reducer, initState} from 'src/store/reducer/auth';

import EncryptedStorage from 'react-native-encrypted-storage';

const Account_Information = ({
  navigation,
  goBackHome,
  userSessionProp,
  dispatch,
}: any) => {
  // const [userSessionState, dispatch] = useReducer(reducer, initState);
  const handleLogout = () => {
    (async () => {
      try {
        await EncryptedStorage.removeItem('token');
        await EncryptedStorage.removeItem('email');
        await EncryptedStorage.removeItem('role');
        dispatch({
          type: 'UPDATE_USER_SESSION',
          payload: {
            userSession: {
              token: null,
              email: null,
              role: null,
            },
            isLogin: false,
          },
        });
        goBackHome();
      } catch (error) {
        console.log(error);
      }
    })();
  };

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.header}>
          <Text style={styles.text_header}>Account Information</Text>
        </View>
        <View style={styles.body}>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#f6f6f6"
            onPress={() => navigation.navigate('Avatar')}>
            <Flex justify="between" style={styles.option_wrapper}>
              <Text style={styles.text_option}>Avatar</Text>
              <Flex justify="between">
                <View style={styles.circle_wrapper}>
                  <Icon
                    size={default_style.icon_size}
                    style={styles.user_icon}
                    name="user"
                  />
                </View>
                <Icon
                  size={20 * default_style.width_ratio}
                  name="arrow-right"
                  style={styles.icon}
                />
              </Flex>
            </Flex>
          </TouchableHighlight>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#f6f6f6"
            onPress={() => navigation.navigate('Fullname')}>
            <Flex justify="between" style={styles.option_wrapper}>
              <Text style={styles.text_option}>Full Name</Text>
              <Flex justify="between">
                <Text style={styles.text_value}>Cody Simons</Text>
                <Icon
                  size={20 * default_style.width_ratio}
                  name="arrow-right"
                  style={styles.icon}
                />
              </Flex>
            </Flex>
          </TouchableHighlight>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#f6f6f6"
            onPress={() => navigation.navigate('Change_Password')}>
            <Flex justify="between" style={styles.option_wrapper}>
              <Text style={styles.text_option}>Change Password</Text>
              <Flex justify="between">
                <Icon
                  size={20 * default_style.width_ratio}
                  name="arrow-right"
                  style={styles.icon}
                />
              </Flex>
            </Flex>
          </TouchableHighlight>
          <View style={styles.divider} />
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#f6f6f6"
            onPress={() => navigation.navigate('Mobile_Number')}>
            <Flex justify="between" style={styles.option_wrapper}>
              <Text style={styles.text_option}>Mobile Number</Text>
              <Flex justify="between">
                <Text style={styles.text_value}>(+84) 370-0738</Text>
                <Icon
                  size={20 * default_style.width_ratio}
                  name="arrow-right"
                  style={styles.icon}
                />
              </Flex>
            </Flex>
          </TouchableHighlight>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#f6f6f6"
            onPress={() => navigation.navigate('Email')}>
            <Flex justify="between" style={styles.option_wrapper}>
              <Text style={styles.text_option}>Email Address</Text>
              <Flex justify="between">
                <Text style={styles.text_value}>
                  {userSessionProp.userSession.email}
                </Text>
                <Icon
                  size={20 * default_style.width_ratio}
                  name="arrow-right"
                  style={styles.icon}
                />
              </Flex>
            </Flex>
          </TouchableHighlight>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#f6f6f6"
            onPress={() => navigation.navigate('Gender')}>
            <Flex justify="between" style={styles.option_wrapper}>
              <Text style={styles.text_option}>Gender</Text>
              <Flex justify="between">
                <Text style={styles.text_value}>Not Set</Text>
                <Icon
                  size={20 * default_style.width_ratio}
                  name="arrow-right"
                  style={styles.icon}
                />
              </Flex>
            </Flex>
          </TouchableHighlight>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#f6f6f6"
            onPress={() => navigation.navigate('Birthday')}>
            <Flex justify="between" style={styles.option_wrapper}>
              <Text style={styles.text_option}>Birthday</Text>
              <Flex justify="between">
                <Text style={styles.text_value}>Not Set</Text>
                <Icon
                  size={20 * default_style.width_ratio}
                  name="arrow-right"
                  style={styles.icon}
                />
              </Flex>
            </Flex>
          </TouchableHighlight>
        </View>
      </View>
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.log_out_wrapper}
        onPress={handleLogout}>
        <View style={styles.log_out}>
          <Icon
            size={default_style.icon_size}
            name="logout"
            style={styles.icon_log_out}
          />
          <Text style={styles.text_log_out}>Log out</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Account_Information;
