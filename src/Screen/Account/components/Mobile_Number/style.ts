import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: default_style.white,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    height: '100%',
  },
  text: {
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_600,
    color: default_style.black,
    lineHeight: default_style.line_height_normal,
  },
  icon: {
    color: default_style.black,
    marginRight: 10 * default_style.width_ratio,
  },
});
