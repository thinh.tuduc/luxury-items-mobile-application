import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  icon: {
    marginRight: 9 * default_style.width_ratio,
  },
  text: {
    color: '#7a7a7a',
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_500,
    lineHeight: default_style.line_height_normal,
  },
  sidebar_item_padding: {
    paddingVertical: 20 * default_style.height_ratio,
    paddingHorizontal: 10 * default_style.width_ratio,
  },
  background_focus: {
    backgroundColor: default_style.light_sun,
  },
  default_color: {
    color: '#7a7a7a',
  },
  icon_color: {
    color: '#F1E1CD',
  },
  text_color: {
    color: default_style.black,
  },
});
