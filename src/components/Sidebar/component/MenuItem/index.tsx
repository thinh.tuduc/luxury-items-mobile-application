import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

const MenuItem = ({moveTo, state, index, name, iconName}: any) => {
  return (
    <>
      <TouchableOpacity onPress={moveTo}>
        <View
          style={
            state.index === index
              ? [styles.sidebar_item_padding, styles.background_focus]
              : styles.sidebar_item_padding
          }>
          <Flex justify="start">
            <Icon
              size={default_style.icon_size}
              name={iconName}
              style={
                state.index === index
                  ? [styles.icon, styles.icon_color]
                  : [styles.icon, styles.default_color]
              }
            />
            <Text
              style={
                state.index === index
                  ? [styles.text, styles.text_color]
                  : [styles.text, styles.default_color]
              }>
              {name}
            </Text>
          </Flex>
        </View>
      </TouchableOpacity>
    </>
  );
};

export default MenuItem;
