/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useState, useReducer} from 'react';
import {Text, View, TouchableOpacity, SafeAreaView} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

import MenuItem from './component/MenuItem';

const Sidebar = (props: any) => {
  const {state} = props;
  // const [userSessionState, dispatch] = useReducer(reducer, initState);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.body}>
        <View style={styles.header}>
          {props.userSession.userSession.role === 'vendor' ? (
            <>
              <View style={styles.sidebar_group_wrapper}>
                <MenuItem
                  moveTo={() => props.navigation.navigate('Dashboard')}
                  state={state}
                  index={0}
                  name="Dashboard"
                  iconName="home"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Products')}
                  state={state}
                  index={2}
                  name="Products"
                  iconName="shop"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Report')}
                  state={state}
                  index={4}
                  name="Report"
                  iconName="bar-chart"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Livestream')}
                  state={state}
                  index={3}
                  name="Livestream"
                  iconName="play-circle"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Luxuria')}
                  state={state}
                  index={1}
                  name="Luxuria"
                  iconName="file-text"
                />
              </View>
              <View style={styles.sidebar_group_wrapper}>
                <MenuItem
                  moveTo={() => props.navigation.navigate('AdCenter')}
                  state={state}
                  index={5}
                  name="Ad Center"
                  iconName="notification"
                />
              </View>
              <View
                style={[styles.sidebar_group_wrapper, styles.no_border_bottom]}>
                <MenuItem
                  moveTo={() => props.navigation.navigate('Notification')}
                  state={state}
                  index={6}
                  name="Notification"
                  iconName="bell"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Settings')}
                  state={state}
                  index={7}
                  name="Settings"
                  iconName="setting"
                />
              </View>
            </>
          ) : (
            <>
              <View style={styles.sidebar_group_wrapper}>
                <MenuItem
                  moveTo={() => props.navigation.navigate('Home')}
                  state={state}
                  index={0}
                  name="Home"
                  iconName="home"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Luxuria')}
                  state={state}
                  index={1}
                  name="Luxuria"
                  iconName="file-text"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Categories')}
                  state={state}
                  index={2}
                  name="Categories"
                  iconName="appstore"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Livestream')}
                  state={state}
                  index={3}
                  name="Livestream"
                  iconName="play-circle"
                />
              </View>
              <View
                style={[styles.sidebar_group_wrapper, styles.no_border_bottom]}>
                <MenuItem
                  moveTo={() => props.navigation.navigate('Favorites')}
                  state={state}
                  index={4}
                  name="Favorites"
                  iconName="heart"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Chat')}
                  state={state}
                  index={5}
                  name="Chat"
                  iconName="message"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Notification')}
                  state={state}
                  index={6}
                  name="Notification"
                  iconName="bell"
                />
                <MenuItem
                  moveTo={() => props.navigation.navigate('Settings')}
                  state={state}
                  index={7}
                  name="Settings"
                  iconName="setting"
                />
              </View>
            </>
          )}
        </View>
        <TouchableOpacity
          onPress={() => {
            if (props.userSession.isLogin) {
              props.navigation.navigate('Account');
            } else {
              props.navigation.navigate('Login');
            }
          }}>
          <View style={styles.user_wrapper}>
            <Flex justify="between">
              <Flex justify="start">
                <View style={styles.circle_wrapper}>
                  <Icon
                    size={default_style.icon_size}
                    style={styles.user_icon}
                    name="user"
                  />
                </View>
                <Flex direction="column" align="start">
                  <Text numberOfLines={1} style={styles.text_name}>
                    {!props.userSession.isLogin
                      ? 'Sign up / Log in'
                      : props.userSession.userSession.email}
                  </Text>
                  {/* <Text style={styles.text_phone}>(+84) 370-0738</Text> */}
                </Flex>
              </Flex>
              <Icon
                size={default_style.icon_size}
                name={!props.userSession.isLogin ? 'login' : 'edit'}
                style={styles.default_color}
              />
            </Flex>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Sidebar;
