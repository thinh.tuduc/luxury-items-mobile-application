import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10 * default_style.width_ratio,
    // paddingVertical: 0 * default_style.height_ratio,
    backgroundColor: default_style.black,
    height: '100%',
    flex: 1,
  },
  body: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  header: {
    width: '100%',
    flex: 1,
  },
  user_wrapper: {
    width: '100%',
    paddingVertical: 10 * default_style.height_ratio,
    paddingHorizontal: 10 * default_style.width_ratio,
    marginBottom: 10 * default_style.height_ratio,
    backgroundColor: '#383838',
  },
  circle_wrapper: {
    width: 32 * default_style.width_ratio,
    height: 32 * default_style.height_ratio,
    borderRadius: 16 * default_style.width_ratio,
    backgroundColor: '#f9f8f8',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 9 * default_style.width_ratio,
  },
  user_icon: {
    color: default_style.black,
  },
  icon: {
    marginRight: 9 * default_style.width_ratio,
  },
  text: {
    color: '#7a7a7a',
    fontSize: default_style.lg,
    fontFamily: default_style.font_family_500,
    lineHeight: default_style.line_height_normal,
  },
  text_name: {
    width: 135,
    fontSize: default_style.md,
    fontFamily: default_style.font_family_normal,
    color: default_style.white,
    lineHeight: default_style.line_height_normal,
  },
  text_phone: {
    fontSize: default_style.sm,
    fontFamily: default_style.font_family_normal,
    color: default_style.gray,
    lineHeight: default_style.line_height_normal - 4,
  },
  sidebar_group_wrapper: {
    paddingVertical: 15 * default_style.height_ratio,
    borderBottomWidth: 1,
    borderBottomColor: default_style.light_black,
  },
  sidebar_item_padding: {
    paddingVertical: 20 * default_style.height_ratio,
    paddingHorizontal: 10 * default_style.width_ratio,
  },
  logout_wrapper: {
    paddingVertical: 20 * default_style.height_ratio,
  },
  background_focus: {
    backgroundColor: default_style.light_sun,
  },
  default_color: {
    color: '#7a7a7a',
  },
  icon_color: {
    color: '#F1E1CD',
  },
  text_color: {
    color: default_style.black,
  },
  no_border_bottom: {
    borderBottomWidth: 0,
  },
});
