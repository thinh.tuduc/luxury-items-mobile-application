import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  card: {
    borderWidth: 1,
    borderColor: '#DCDCDC',
    borderRadius: 4,
  },
  header: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    borderBottomWidth: 1,
    borderBottomColor: '#DCDCDC',
  },
  body: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    borderBottomWidth: 1,
    borderBottomColor: '#DCDCDC',
  },
  footer: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
    borderBottomWidth: 1,
    borderBottomColor: '#DCDCDC',
  },
  extra_footer: {
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: default_style.standard_padding_vertical,
  },
});
