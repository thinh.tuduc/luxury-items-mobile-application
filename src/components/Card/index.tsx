import React from 'react';
import {View} from 'react-native';

import {styles} from './style';

const Card = ({header, body, footer, extraFooter}: any) => {
  return (
    <View style={styles.card}>
      {header && <View style={styles.header}>{header}</View>}
      <View style={styles.body}>{body}</View>
      <View style={styles.footer}>{footer}</View>
      {extraFooter && <View style={styles.extra_footer}>{extraFooter}</View>}
    </View>
  );
};

export default Card;
