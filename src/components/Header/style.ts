import {StyleSheet, Platform} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  headerWrapper: {
    // flex: 1,
    backgroundColor:
      Platform.OS === 'ios' ? default_style.white : default_style.black,
    // height: '100%',
    // marginTop: 50,
  },
  container: {
    width: '100%',
    height: 56 * default_style.height_ratio,
    // backgroundColor: default_style.black,
    paddingHorizontal: default_style.standard_padding_horizontal,
    paddingVertical: 10,
  },
  icon: {
    // color: '#fcfaf6',
    color: Platform.OS === 'ios' ? default_style.black : '#fcfaf6',
  },
  shopIcon: {
    marginLeft: 20,
  },
  text: {
    color: default_style.sun,
    fontSize: default_style.xl,
    fontFamily: default_style.font_family_500,
    marginLeft: 20,
  },
});
