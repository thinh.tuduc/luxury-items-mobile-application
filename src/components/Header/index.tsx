import React from 'react';
import {Text, TouchableOpacity, SafeAreaView} from 'react-native';
import {Icon, Flex} from '@ant-design/react-native';
import {default_style} from 'src/constant/default_style';

import {styles} from './style';

interface MenuProp {
  onPress: () => {};
  onClickCart: () => void;
  userSession: any;
}

const Menu = ({onPress, onClickCart, userSession}: MenuProp) => {
  return (
    <>
      <SafeAreaView style={styles.headerWrapper}>
        <Flex style={styles.container} justify="between">
          <TouchableOpacity onPress={onPress}>
            <Icon
              name="menu"
              size={default_style.icon_size}
              style={styles.icon}
            />
          </TouchableOpacity>
          <Text style={styles.text}>LUXURY ITEMS</Text>
          <Flex justify="end">
            {userSession.userSession.role === 'vendor' ? (
              <>
                <Icon
                  name="message"
                  size={default_style.icon_size}
                  style={styles.icon}
                />
                <TouchableOpacity onPress={onClickCart}>
                  <Icon
                    name="inbox"
                    size={default_style.icon_size}
                    style={[styles.shopIcon, styles.icon]}
                  />
                </TouchableOpacity>
              </>
            ) : (
              <>
                <Icon
                  name="search"
                  size={default_style.icon_size}
                  style={styles.icon}
                />
                <TouchableOpacity onPress={onClickCart}>
                  <Icon
                    name="shopping-cart"
                    size={default_style.icon_size}
                    style={[styles.shopIcon, styles.icon]}
                  />
                </TouchableOpacity>
              </>
            )}
          </Flex>
        </Flex>
      </SafeAreaView>
    </>
  );
};

export default Menu;
