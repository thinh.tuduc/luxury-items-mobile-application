import React from 'react';
import {View, Text} from 'react-native';

import {styles} from './style';

interface tagProp {
  content: string;
}

const Tag = ({content}: tagProp) => {
  return (
    <View style={styles.tag}>
      <Text style={styles.text_tag}>{content}</Text>
    </View>
  );
};

export default Tag;
