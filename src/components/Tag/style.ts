import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  tag: {
    backgroundColor: '#F8F0E6',
    borderWidth: 1,
    borderColor: '#EBD2B3',
    borderRadius: 2 * default_style.width_ratio,
    paddingHorizontal: 5 * default_style.width_ratio,
    paddingVertical: 5 * default_style.height_ratio,
    marginRight: 10 * default_style.width_ratio,
  },
  text_tag: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: 20 * default_style.height_ratio,
    color: '#C79A6F',
  },
});
