import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';

export const styles = StyleSheet.create({
  table_wrapper: {
    width: '100%',
    // height: 400 * default_style.height_ratio,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#EAEAEA',
  },
  cell: {
    width: 50 * default_style.height_ratio,
    height: 60 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#EAEAEA',
  },
  bodyCell: {
    // width: 200 * default_style.height_ratio,
    height: 60 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#EAEAEA',
    paddingHorizontal: 7 * default_style.width_ratio,
  },
  extraCell: {
    width: 50 * default_style.height_ratio,
    height: 60 * default_style.height_ratio,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#EAEAEA',
  },
  header: {
    backgroundColor: '#F6F6F6',
  },
  bodyHeader: {
    backgroundColor: '#E7E7E7',
  },
  text: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_header: {
    fontFamily: default_style.font_family_500,
    fontSize: default_style.md,
    lineHeight: default_style.line_height_normal,
    color: default_style.light_black,
  },
  text_small: {
    fontFamily: default_style.font_family_normal,
    fontSize: default_style.sm,
    lineHeight: default_style.line_height_normal,
    color: default_style.gray,
  },
});
