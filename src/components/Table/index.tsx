import React, {useState} from 'react';
import {Text, View, ScrollView} from 'react-native';
import {Flex, Icon} from '@ant-design/react-native';
import CheckBox from '@react-native-community/checkbox';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';
// import {default_style} from 'src/constant/default_style';

interface TableProp {
  field: any;
  fieldData: any;
  type: 'product' | 'order' | 'normal';
  dataWidth: any;
}

const Table = ({field, fieldData, type, dataWidth}: TableProp) => {
  const [toggleCheckBox, setToggleCheckBox] = useState<boolean>(false);
  return (
    <>
      <View style={styles.table_wrapper}>
        <Flex align="start">
          <Flex direction="column" align="start">
            <View style={[styles.cell, styles.header]}>
              <CheckBox
                tintColors={{true: '#ddb481'}}
                disabled={false}
                value={toggleCheckBox}
                onValueChange={newValue => setToggleCheckBox(newValue)}
              />
            </View>
            {fieldData.map((x: any, id: number) => {
              return (
                <View key={id} style={styles.cell}>
                  <CheckBox
                    tintColors={{true: '#ddb481'}}
                    disabled={false}
                    value={toggleCheckBox}
                    onValueChange={newValue => setToggleCheckBox(newValue)}
                  />
                </View>
              );
            })}
          </Flex>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <Flex direction="column" align="start">
              <Flex>
                {field.map((x: any, id: number) => {
                  return (
                    <View
                      key={id}
                      style={[
                        styles.bodyCell,
                        styles.header,
                        {width: x.width * default_style.width_ratio},
                      ]}>
                      <Text style={styles.text_header}>{x.name}</Text>
                    </View>
                  );
                })}
              </Flex>
              {type === 'product' &&
                fieldData.map((x: any, id: number) => {
                  return (
                    <Flex key={id}>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[0] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.category}</Text>
                      </View>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[1] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.name}</Text>
                      </View>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[2] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.brand}</Text>
                      </View>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[3] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.date}</Text>
                      </View>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[4] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.stock}</Text>
                      </View>
                    </Flex>
                  );
                })}
              {type === 'order' &&
                fieldData.map((x: any, id: number) => {
                  return (
                    <Flex key={id}>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[0] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.orderName}</Text>
                        <Text
                          style={
                            styles.text_small
                          }>{`Order ID: ${x.orderID}`}</Text>
                      </View>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[1] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.date}</Text>
                        <Text style={styles.text_small}>{x.time}</Text>
                      </View>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[2] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.recipient}</Text>
                      </View>
                      <View
                        style={[
                          styles.bodyCell,
                          {width: dataWidth[3] * default_style.width_ratio},
                        ]}>
                        <Text style={styles.text}>{x.totalAmount}</Text>
                      </View>
                    </Flex>
                  );
                })}
            </Flex>
          </ScrollView>
          <Flex direction="column" align="start">
            {type === 'product' && (
              <View style={[styles.extraCell, styles.header]} />
            )}
            {type === 'product' &&
              fieldData.map((x: any, id: number) => {
                return (
                  <View key={id} style={styles.extraCell}>
                    <Icon name="more" />
                  </View>
                );
              })}
            {type === 'order' && (
              <View
                style={[
                  styles.extraCell,
                  styles.header,
                  {width: 130 * default_style.width_ratio},
                ]}>
                <Text style={styles.text_header}>Status</Text>
              </View>
            )}
            {type === 'order' &&
              fieldData.map((x: any, id: number) => {
                return (
                  <View
                    key={id}
                    style={[
                      styles.extraCell,
                      {width: 130 * default_style.width_ratio},
                    ]}>
                    <Text style={styles.text}>{x.status}</Text>
                  </View>
                );
              })}
          </Flex>
        </Flex>
      </View>
    </>
  );
};

export default Table;
