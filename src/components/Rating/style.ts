import {StyleSheet} from 'react-native';
import {default_style} from 'src/constant/default_style';
export const styles = StyleSheet.create({
  iconFill: {
    color: default_style.light_sun,
  },
  iconEmpty: {
    color: '#CCD3DB',
  },
  rating: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
