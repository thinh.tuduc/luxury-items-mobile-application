import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
// import {Icon} from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {styles} from './style';
import {default_style} from 'src/constant/default_style';

interface RatingProp {
  defaultValue: number;
  numberStar: number[];
  onChange: (value: number) => void;
  disabled: boolean;
  size: number;
}

const Rating = ({
  defaultValue,
  numberStar,
  onChange,
  disabled,
  size,
}: RatingProp) => {
  const [defaultRating, setDefaultRating] = useState<number>(defaultValue);
  const maxRating = numberStar;

  return (
    <View style={styles.rating}>
      {maxRating.map((x, id) => {
        return (
          <TouchableOpacity
            activeOpacity={0.5}
            disabled={disabled}
            key={id}
            onPress={() => {
              setDefaultRating(x);
              onChange(x);
            }}>
            <Icon
              name="star"
              size={size * default_style.width_ratio}
              style={x <= defaultRating ? styles.iconFill : styles.iconEmpty}
            />
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default Rating;
