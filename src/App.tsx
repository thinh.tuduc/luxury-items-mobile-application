/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useState, useEffect, useReducer} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import Header from './components/Header';
import Sidebar from './components/Sidebar';
import Dashboard from './Screen/Dashboard';
import Report from './Screen/Report';
import ProductManagement from './Screen/ProductManagement';
import OrderManagement from './Screen/OrderManagement';
import AdCenter from './Screen/AdCenter';
import Home from './Screen/Home';
import Favorites from './Screen/Favorites';
import Luxuria from './Screen/Luxuria';
import Livestream from './Screen/Livestream';
import Settings from './Screen/Settings';
import Categories from './Screen/Categories';
import Chat from './Screen/Chat';
import ChatDetail from './Screen/ChatDetail';
import Notification from './Screen/Notification';
import Account from './Screen/Account';
import Order from './Screen/Order';
import Product from './Screen/Product';
import Login from './Screen/Login';
import Signup from './Screen/Signup';
import Splash from './Screen/Splash';

import {default_style} from './constant/default_style';

import {reducer, initState} from 'src/store/reducer/auth';

import EncryptedStorage from 'react-native-encrypted-storage';

import {LogBox} from 'react-native';

const Drawer = createDrawerNavigator();

const App = () => {
  const [isSplash, setSplash] = useState<boolean>(false);
  const [userSessionState, dispatch] = useReducer(reducer, initState);

  const options = ({navigation}: any) => ({
    header: () => (
      <Header
        userSession={userSessionState}
        onClickCart={() => {
          if (userSessionState.userSession.role === 'vendor') {
            navigation.navigate('OrderManagement');
          } else {
            navigation.navigate('Order');
          }
        }}
        onPress={() => navigation.openDrawer()}
      />
    ),
    drawerStyle: {
      width: 250 * default_style.width_ratio,
    },
  });

  useEffect(() => {
    LogBox.ignoreAllLogs();
    setInterval(() => {
      setSplash(true);
    }, 2000);
  }, []);

  useEffect(() => {
    (async () => {
      try {
        const token = await EncryptedStorage.getItem('token');
        const email = await EncryptedStorage.getItem('email');
        const role = await EncryptedStorage.getItem('role');
        dispatch({
          type: 'UPDATE_USER_SESSION',
          payload: {
            userSession: {
              token,
              email,
              role,
            },
            isLogin: token ? true : false,
          },
        });
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  return (
    <>
      {isSplash ? (
        <NavigationContainer>
          <Drawer.Navigator
            initialRouteName={
              userSessionState.userSession.role === 'vendor'
                ? 'Dashboard'
                : 'Home'
            }
            drawerContent={props => (
              <Sidebar userSession={userSessionState} {...props} />
            )}
            screenOptions={options}>
            {userSessionState.userSession.role === 'vendor' ? (
              <Drawer.Screen name="Dashboard" component={Dashboard} />
            ) : (
              <Drawer.Screen name="Home" component={Home} />
            )}
            <Drawer.Screen name="Luxuria" component={Luxuria} />
            {userSessionState.userSession.role === 'vendor' ? (
              <Drawer.Screen name="Products" component={ProductManagement} />
            ) : (
              <Drawer.Screen name="Categories" component={Categories} />
            )}
            <Drawer.Screen name="Livestream" component={Livestream} />
            {userSessionState.userSession.role === 'vendor' ? (
              <Drawer.Screen name="Report" component={Report} />
            ) : (
              <Drawer.Screen name="Favorites" component={Favorites} />
            )}
            {userSessionState.userSession.role === 'vendor' ? (
              <Drawer.Screen name="AdCenter" component={AdCenter} />
            ) : (
              <Drawer.Screen name="Chat" component={Chat} />
            )}
            <Drawer.Screen name="Notification" component={Notification} />
            <Drawer.Screen name="Settings" component={Settings} />
            <Drawer.Screen name="Account">
              {props => (
                <Account
                  dispatch={dispatch}
                  userSession={userSessionState}
                  {...props}
                />
              )}
            </Drawer.Screen>
            {userSessionState.userSession.role === 'vendor' ? (
              <Drawer.Screen
                name="OrderManagement"
                component={OrderManagement}
              />
            ) : (
              <Drawer.Screen name="Order">
                {props => <Order userSession={userSessionState} {...props} />}
              </Drawer.Screen>
            )}
            <Drawer.Screen name="Product" component={Product} />
            <Drawer.Screen name="Login" options={{headerShown: false}}>
              {props => <Login dispatch={dispatch} {...props} />}
            </Drawer.Screen>
            <Drawer.Screen
              name="Signup"
              component={Signup}
              options={{headerShown: false}}
            />
            <Drawer.Screen
              name="ChatDetail"
              component={ChatDetail}
              options={{headerShown: false}}
            />
          </Drawer.Navigator>
        </NavigationContainer>
      ) : (
        <Splash />
      )}
    </>
  );
};

export default App;
