import axios from '../axios';
// import {BASE_URL} from 'src/baseUrl';

interface LoginResponseType {
  token: string;
  email: string;
  role: 'vendor' | 'buyer';
}

export const login = async (email: string, password: string) => {
  const response = await axios.post('/auth/login', {
    email,
    password,
  });
  return response;
};

export const signup = async (email: string, password: string, role: string) => {
  const response = await axios.post('/auth/signup', {
    email,
    password,
    role,
  });
  return response;
};
