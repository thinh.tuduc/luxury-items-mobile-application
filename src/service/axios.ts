import axios, {AxiosError} from 'axios';

import {BASE_URL} from '@env';

const axiosBase = axios.create();

axiosBase.defaults.baseURL = BASE_URL;

axiosBase.interceptors.response.use(
  response => {
    return response;
  },

  (error: AxiosError) => {
    if (error.response) {
      return Promise.reject(error.response.data);
    }
    return Promise.reject(error);
  },
);

export default axiosBase;
