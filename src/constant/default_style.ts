import {Dimensions, Platform} from 'react-native';

const width_ratio = Dimensions.get('screen').width / 411;
const height_ratio = Dimensions.get('screen').height / 914;

export const default_style = {
  //ratio
  width_ratio,
  height_ratio,
  column_ratio: 2 / 411,
  //color
  sun: '#E1AA4D',
  light_sun: '#DDB481',
  black: '#222222',
  light_black: '#555555',
  gray: '#9E9E9E',
  white: '#FFFFFF',
  //screen
  screen_width: Dimensions.get('screen').width,
  screen_height: Dimensions.get('screen').height,
  //font size
  sm: 12 * width_ratio,
  md: 14 * width_ratio,
  lg: 16 * width_ratio,
  xl: 18 * width_ratio,
  xxl: 20 * width_ratio,
  fz38: 38 * width_ratio,
  //line height
  line_height_normal: 24 * height_ratio,
  line_height_20: 20 * height_ratio,
  line_height_48: 48 * height_ratio,
  //icon size
  icon_size: 20 * width_ratio,
  //font family
  font_family_normal:
    Platform.OS === 'ios' ? 'WorkSans-Regular' : 'work-sans.normal',
  font_family_500: Platform.OS === 'ios' ? 'WorkSans-Medium' : 'work-sans.500',
  font_family_600:
    Platform.OS === 'ios' ? 'WorkSans-SemiBold' : 'work-sans.600',
  font_family_700: Platform.OS === 'ios' ? 'WorkSans-Bold' : 'work-sans.700',
  // padding
  standard_padding_horizontal: 15,
  standard_padding_vertical: 15,
};
